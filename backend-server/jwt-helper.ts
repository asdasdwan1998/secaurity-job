import { Bearer } from "permit";
import express from "express";
import { userService } from "./services";
import { JWTPayload } from "./models";
import { HttpError } from "./http-error";

const permit = new Bearer({
  query: "access_token",
});

export function requireJWTPayload(req: express.Request) {
  let token: string;
  try {
    token = permit.check(req);
  } catch (error) {
    throw new HttpError("Invalid Bearer Token", 401);
  }
  return userService.decodeToken(token);
}

export async function requireCompanyJWT(req: express.Request) {
  let jwt = await requireJWTPayload(req);
  let company_id = jwt.payload.company_id;
  if (!company_id) {
    throw new HttpError("not company owner", 401);
  }
  if(! jwt.payload.enable){
    throw new HttpError("company not approve", 401);
  }
  return jwt.payload;
}

export async function requireAdminJWT(req: express.Request) {
  let jwt = await requireJWTPayload(req);
  if(! jwt.payload.is_admin){
    throw new HttpError("user is not admin", 401);
  }
  if(! jwt.payload.enable){
    throw new HttpError("admin not approve", 401);
  }
  return jwt;
}