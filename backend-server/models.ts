export type JWTPayload = {
    id: number 
    exp: number //expire_at (ms)
    is_admin: true
    nickname: string
    company_id: number
    enable : boolean
}
| {
    id: number  
    is_admin: false
    nickname: string
    company_id: number
    enable : boolean
}

export type Employee = {
    gender? : string
    salary? : number
    attach? : any
    bank_account? : string
    fullname: string
}

export type Company = {
    name: string
    telephone?: string
    address?: string
    email?: string
    bank_account?: string
    principal?: string
}

export type Job = {
    name: string;
    salary: number;
    district_id: number;
    company_id : number;
    admin_id: number;
    latlng: any;
    place: string;
    require_worker_num: number;
    other?: string;
    start_date:string;
    end_date:string;
    daily_start_time:string;
    daily_end_time:string

}