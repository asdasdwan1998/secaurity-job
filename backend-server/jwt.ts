import { env } from './env'

export let jwtConfig = {
    jwtSecret: env.jwt_secret,
    jwtSession: {
        session: false
    }
}