import { Endpoint } from 'aws-sdk'
import { createTransport } from 'nodemailer'
import { env } from '../env'

const EMAIL_INTERVAL = 1000 * 60 * 3 // 3 minutes

export class EmailService {
  private transporter = createTransport({
    host: env.email.EMAIL_HOST,
    port: env.email.EMAIL_PORT,
    auth: {
      user: env.email.EMAIL_USER,
      pass: env.email.EMAIL_PASSWORD,
    },
  })
  // to avoid spamming the email too often
  private email_cool_down_set = new Set<string>()

  public async sendEmail(email: {
    email_address: string
    title: string
    body: string
  }) {
  
    if (email.email_address.endsWith("@example.net")) {
        // console.log(email)
        return 
    }
    let address = email.email_address
    if (this.email_cool_down_set.has(address)) {
      throw new Error('Sending too frequently, please retry later')
    }
    await this.transporter.sendMail({
      from: env.email.EMAIL_USER,
      to: email.email_address,
      subject: email.title,
      html: email.body,
    })
    this.email_cool_down_set.add(address)
    setTimeout(() => {
      this.email_cool_down_set.delete(address)
    }, EMAIL_INTERVAL)
  }
}
