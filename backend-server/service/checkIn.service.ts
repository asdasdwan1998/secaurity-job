import { Knex } from "knex";
import { HttpError } from "../http-error";

export class CheckInService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("check_in");
  }
  async createCheckIn(job_role_id: number, position: any): Promise<number> {
    let result = await this.checkAlreadyCheckIn(job_role_id)
    if(result){throw new HttpError("please check out first",400)}
    let time = new Date(Date.now());
    let row = await this.table()
      .insert({ job_role_id, position, time })
      .returning("id");
    return row[0].id as number;
  }

  async checkAlreadyCheckIn(job_role_id: number) {
    let result = await this.table()
      .select("employee_id", "job_id", "check_in.time")
      .innerJoin("job_role", "job_role_id", "job_role.id")
      .leftJoin("check_out", "check_in_id", "check_in.id")
      .where({ job_role_id }).whereNull("check_out.time")
    if(!result){
      return null
    }
    return result
  }
}

export class CheckOutService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("check_out");
  }
  async createCheckOut(check_in_id: number, position: any): Promise<number> {
    let time = new Date(Date.now());
    let row = await this.table()
      .insert({ check_in_id, position, time })
      .returning("id");
    return row[0].id as number;
  }
}
