import { Knex } from "knex";
import { HttpError } from "../http-error";

export class AdminService {
  constructor(private knex: Knex) { }
  table() {
    return this.knex("admin");
  }
  async createAdmin(user_id: number) {

    let row = await this.table().select("user_id").where({ user_id }).first();
    if (!row) {
      await this.table().insert({ user_id }).returning("user_id");
    }
  }

  async deleteAdmin(user_id: number) {
    let delete_time = new Date(Date.now())
    await this.table().update({ delete_time }).where({ user_id }).first()
    return { message: user_id + " has been delete admin role at " + delete_time }
  }
}
