import { Knex } from "knex";
import { HttpError } from "../http-error";
import { Job } from "../models";

export class JobService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("job");
  }

  async createJob(job: Job) {
    let row = await this.table().insert(job).returning("id");
    let job_id= row[0].id;
    return job_id
    
  }
  


  // async getAllJobDetail() {
  //   let row = await this.table()
  //     .select(
  //       "name",
  //       "salary",
  //       "place",
  //       "require_worker_num",
  //       "startDate",
  //       "endDate",
  //       "dailyStartTime",
  //       "dailyEndTime"
  //     )
  //     .where(
  //       "",
  //     )
  //     .innerJoin("job_time", "job.id", "job_id")
  //   if (!row) {
  //     throw new HttpError("user row not found", 404);
  //   }
  //   return row;
  // }


  async getJobList(company_id:number) {
    let row = await this.table()
      .select(
        "id",
        "admin_id",
        "district_id",
        "company_id",
        "name",
        "salary",
        "latlng",
        "place",
        "require_worker_num",
        "other",        
        "start_date",
        "end_date",
        "daily_start_time",
        "daily_end_time",
      )
    .where({company_id})

    if (!row) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }
  async getFinishJobList(company_id:number) {
    let today = new Date().toISOString()
    let row = await this.table()
      .select(
        "id",
        "admin_id",
        "district_id",
        "company_id",
        "name",
        "salary",
        "latlng",
        "place",
        "require_worker_num",
        "other",        
        "start_date",
        "end_date",
        "daily_start_time",
        "daily_end_time",
      )
      .where("end_date" ,"<" ,today)
      .where({company_id})

    if (!row) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }
  async getNotYetJobList(company_id:number) {
    let today = new Date().toISOString()
    let row = await this.table()
      .select(
        "id",
        "admin_id",
        "district_id",
        "company_id",
        "name",
        "salary",
        "latlng",
        "place",
        "require_worker_num",
        "other",        
        "start_date",
        "end_date",
        "daily_start_time",
        "daily_end_time",
      )
      .where("end_date" ,">" ,today)
      .where({company_id})

    return row;
    }
      async getEmployeeJobList() {
        let today = new Date().toISOString()
        let row = await this.table()
          .select(
            "id",
            "admin_id",
            "district_id",
            "company_id",
            "name",
            "salary",
            "latlng",
            "place",
            "require_worker_num",
            "other",        
            "start_date",
            "end_date",
            "daily_start_time",
            "daily_end_time",
          )
          .where("end_date" ,">" ,today)


    if (!row) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }

  async getSpecificJobList(company_id:number,startDate:Date,endDate:Date) {
    let row = await this.table()
      .select(
        "id",
        "name",
        "salary",
        "place",
        "require_worker_num",
        "start_date",
        "end_date",
        "daily_start_time",
        "daily_end_time"
      )
      .where({company_id})
      .where("end_date" ,">" ,startDate)
      .andWhere("end_date","<",endDate)

    if (!row) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }

  async getJobIdByName(name: string) {
    let row = await this.table().select("id").where({ name }).first();
    if (!row) {
      return null;
    }
    return row.id as number;
  }

  async getRoomName(id: number){
    let row = await this.table().select("name as roomName").where({ id }).first();
    if (!row) {
      return null;
    }
    return row
  }
}
 




export class JobTimeService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("job_time");
  }

  async createJobTime(job_id: number, start_time: Date, end_time: Date) {
    await this.table().insert({ job_id, start_time, end_time });
  }

  async deleteJobTime(id: number) {
    await this.table().delete().where({ id });
  }
}

export class JobFileService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("job_file");
  }
  async createJobFile(job_id: number, filename: string) {
    let minetype = filename.split(".").pop();
    let row = await this.table()
      .insert({ job_id, filename, minetype })
      .returning("id");
    return row[0].id;
  }

  async deleteJobFile(filename: string) {
    await this.table().delete().where({ filename });
    return { message: "delete OK" };
  }

  async selectJobFile(job_id: number) {
    return await this.table().select().where({ job_id });
  }
}
