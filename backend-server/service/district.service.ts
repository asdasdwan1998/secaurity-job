import { Knex } from "knex";
import { HttpError } from "../http-error";

export class DistrictService {
  constructor(private knex: Knex) { }
  table() {
    return this.knex("district");
  }
  async createDistrict(name: string) {
    let row = await this.table().select("id").where({ name }).first();


    if (!row) {
      let result: any[] = await this.table().insert({ name }).returning("id");
      return result[0].id as number;
    }
    return row.id as number
  }

  async getDistrict(name: string) {
    let row = await this.table().select("id").where({ name }).first();
    if (!row) {
      throw new HttpError("district not found", 404);
    }
    return row.id as number;
  }

  async getDistrictList() {
    return await this.table().select()
  }
}


export class EmployeeDistrictService {
  constructor(private knex: Knex) { }
  table() {
    return this.knex("employee_district");
  }
  async createEmployeeDistrict(user_id: number, district_list: number[]) {
    await this.knex.transaction(async (knex) => {
      await knex('employee_district').where({ user_id }).delete()
      await knex('employee_district').insert(district_list.map(district_id => ({ user_id, district_id })))
    }
    )
  }

  async deleteAllEmployeeDistrict(user_id: number) {
    let row = await this.table().delete().where({ user_id })
    if (!row) {
      throw new HttpError("Employee District not found", 404);
    }
    return true
  }

  async getEmployeeDistrict(user_id: number){
    const rows = await this.table().select("district_id").where({ user_id })
    const idList = rows.map(row=>row.district_id)
    return idList as number[]
  }
}
