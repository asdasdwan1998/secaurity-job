import { Knex } from "knex";
import { HttpError } from "../http-error";


export class JobRoleService {
    constructor(private knex: Knex) { }
    table() {
        return this.knex("job_role");
    }
    async getJobRoleID(job_id: number, employee_id: number){
        let row = await this.table().select("id").where({job_id, employee_id}).first()
        if(!row){
            return null
        }
        return row.id as number
    }

    async createJobRole(job_id: number, employee_id: number) {
        let jobRoleID = await this.getJobRoleID(job_id,employee_id )
        if(jobRoleID){ return jobRoleID as number}
        let row = await this.table().insert({ job_id, employee_id }).returning("id");
        return row[0].id as number;
    }

    async confirmJobRole(job_id: number, employee_id: number) {
        let confirm_time = new Date()
        let result = await this.table().update({ confirm_time:confirm_time}).where({ job_id:job_id, employee_id:employee_id }).returning('id')
        if (!result) {
            throw new HttpError("update failed", 400)
        }
        return { message: "update success" }
    }
    async confirmJobRoleByID(id: number) {
        let confirm_time = new Date()
        let result = await this.table().update({ confirm_time }).where({ id })
        if (!result) {
            throw new HttpError("update failed", 400)
        }
        return { message: "update success" }
    }
    async rejectJobRole(job_id: number, employee_id: number) {
        let reject_time = new Date(Date.now())
        let result = await this.table().update({ reject_time }).where({ job_id, employee_id }).returning('id')
        if (!result) {
            throw new HttpError("reject failed", 400)
        }
        return { message: "reject success" }
    }
    async rejectJobRoleFromCompany(id: number) {
        let reject_time = new Date(Date.now())
        let result = await this.table().update({ reject_time }).where({ id })
        if (!result) {
            throw new HttpError("reject failed", 400)
        }
        return { message: "reject success" }
    }
    async specialAwardFromCompany(id: number,specialAward:number) {
        let award = specialAward

        let result = await this.table().update({ award }).where({ id })
        if (!result) {
            throw new HttpError("award failed", 400)
        }
        return { message: "reject success" }
    }

    async paymentJobRole(id: number, payment_id: number) {
        let result = await this.table().update({ payment_id }).where({ id })
        if (!result) {
            throw new HttpError("payment failed", 400)
        }
        return { message: "payment success" }
    }

    async scoreJobRole(id: number, score: number) {
        let result = await this.table().update({ score }).where({ id })
        if (!result) {
            throw new HttpError("score failed", 400)
        }
        return { message: "score success" }
    }
    async getJobEmployeeList(job_id: number) {

        let row = await this.table().select("job_role.id","job_role.employee_id", "users.nickname", "employee.fullname")
        .innerJoin("users", "users.id", "job_role.employee_id")
        .innerJoin("job", "job_role.job_id", "job.id")
        .innerJoin("employee", "users.id", "employee.user_id")
        .whereNotNull("confirm_time").whereNull("reject_time")
        .andWhere("job_id", job_id)
        
        return row;
    }

}