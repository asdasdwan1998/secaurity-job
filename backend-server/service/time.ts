export let SECOND = 1000
export let MINUTE = SECOND * 60
export let HOUR = MINUTE * 60
export let DAY = HOUR * 24
export let WEEK = DAY * 7