import { Knex } from "knex";
import { HttpError } from "../http-error";

export class PaymentService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("payment");
  }
  async createPayment(admin_id : number) {
      let payment_time = new Date(Date.now())
      let result:any[] = await this.table().insert({payment_time, admin_id}).returning("id");
      return result[0].id as number;
    }

  async readPayment(id: number) {
    let read_time = new Date(Date.now())
    let row = await this.table().update({read_time}).where({ id })
    if (!row) {
      throw new HttpError("id not found", 404);
    }
    return true
  }

}