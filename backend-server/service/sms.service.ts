import { env } from "../env";
import fetch from 'node-fetch'

export class SmsService {
  public async sendMessage(message: { tel: string; content: string }) {
    console.log(message)
    const url = `https://sms.8x8.com/api/v1/subaccounts/${env.sms.SMS_ACCOUNT_ID}/messages`;
    let res = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + env.sms.SMS_API_KEY,
      },
      body: JSON.stringify({
        source: env.APP_NAME,
        destination: message.tel,
        text: message.content,
        "encoding": "AUTO"
      }),
    });
    if (!res.ok) {
      throw new Error(await res.text());
    }
    console.log(await res.text())
  }
}
