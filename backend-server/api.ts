import express from "express";
import { knex } from "./db";
import {UserService} from "./service/user.service"
import {UserController} from "./controller/user.controller"
import{CompanyRoleService, CompanyService}from"./service/company.service"
import{CompanyController} from"./controller/company.controller"
import{JobController} from"./controller/job.controller"
import{JobRoleController} from"./controller/jobRole.controller"
import{JobFileService, JobService, JobTimeService}from"./service/job.service"
import {EmployeeController} from "./controller/employee.controller"
import { EmployeeService } from "./service/employee.service"
import { AdminController} from "./controller/admin.controller"
import { SmsService } from "./service/sms.service"
import { EmailService } from "./service/email.service"
import { upload } from './upload'
import { DistrictService, EmployeeDistrictService } from "./service/district.service"
import { DistrictController} from "./controller/district.controller"
import { AdminService } from "./service/admin.service"
import { JobRoleService } from "./service/jobRole.service"
import { AwardService, AwardTypeService } from "./service/award.service"
import { CheckInService , CheckOutService } from "./service/checkIn.service"
import { MessageService } from "./service/message.service"
import { PaymentService } from "./service/payment.service"
import { CheckInController } from "./controller/checkIn.controller"
import { AwardController } from "./controller/award.controller";
import { MessageController } from "./controller/message.controller"
import { PaymentController } from "./controller/payment.controller"
import { userService } from "./services";




export let apiRoutes = express.Router();


async function init() {
    let userController = new UserController(userService)
    
    let jobService = new JobService(knex)
    let jobTimeService = new JobTimeService(knex)
    let jobFileService = new JobFileService(knex)
    let jobController = new JobController(jobService,jobTimeService,jobFileService,upload)

    let jobRoleService = new JobRoleService(knex)
    let jobRoleController = new JobRoleController(jobRoleService)

    let employeeDistrictService = new EmployeeDistrictService(knex)
    let employeeService = new EmployeeService(knex)
    let employeeController = new EmployeeController(employeeService,upload,userService, employeeDistrictService)

    let companyService=new CompanyService(knex)
    let companyRoleService=new CompanyRoleService(knex)
    let companyController=new CompanyController(companyService, companyRoleService, userService,upload)

    let adminService=new AdminService(knex)
    let adminController = new AdminController(userService, adminService,companyRoleService,companyService )

    let districtService = new DistrictService(knex)
    let districtController = new DistrictController(districtService)

    let awardService = new AwardService(knex)
    let awardTypeService = new AwardTypeService(knex)
    let awardController = new AwardController(awardService,awardTypeService)

    let checkInService = new CheckInService(knex)
    let checkOutService = new CheckOutService(knex)
    let checkInController = new CheckInController(checkInService,checkOutService)

    let messageService = new MessageService(knex)
    let messageController = new MessageController(messageService, userService, jobService)
    
    let paymentService = new PaymentService(knex)
    let paymentController = new PaymentController(paymentService,awardService,jobRoleService)

    apiRoutes.use(userController.router);
    apiRoutes.use(companyController.router);
    apiRoutes.use(jobController.router);
    apiRoutes.use(jobRoleController.router);
    apiRoutes.use(employeeController.router);
    apiRoutes.use(adminController.router);
    apiRoutes.use(districtController.router);
    apiRoutes.use(awardController.router);
    apiRoutes.use(checkInController.router);
    apiRoutes.use(messageController.router);
    apiRoutes.use(paymentController.router);
}
init();
