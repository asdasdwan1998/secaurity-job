import { RestfulController } from "./rest.controller";
import {CheckInService, CheckOutService} from "../service/checkIn.service"
import express from "express";


export class CheckInController extends RestfulController {
    constructor(private checkInService: CheckInService,
        private checkOutService: CheckOutService){
        super();
        this.router.post("job/checkIn",this.handleRequest(this.createCheckIn));
        this.router.post("job/checkOut",this.handleRequest(this.createCheckOut));
    }
    createCheckIn = async(req:express.Request)=>{
        let {job_role_id,position } = req.body
        return this.checkInService.createCheckIn(job_role_id,position)
    }

    createCheckOut = async(req:express.Request)=>{
        let {check_in_id,position } = req.body
        return this.checkOutService.createCheckOut(check_in_id,position)
    }


}