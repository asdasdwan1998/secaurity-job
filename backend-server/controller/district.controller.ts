import { RestfulController } from "./rest.controller";
import {DistrictService} from "../service/district.service"
import express from "express";


export class DistrictController extends RestfulController {
    constructor(private districtService: DistrictService){
        super();
        this.router.get("/district",this.handleRequest(this.getDistrict));
    }
    getDistrict = async(req:express.Request)=>{
        return this.districtService.getDistrictList()
    }
}