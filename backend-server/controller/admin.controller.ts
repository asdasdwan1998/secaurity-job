import express from "express";
import { HttpError } from "../http-error";
import { UserService } from "../service/user.service";
import { AdminService } from "../service/admin.service";
import { RestfulController } from "./rest.controller";
import { requireJWTPayload } from "../jwt-helper";
import { CompanyRoleService, CompanyService } from "../service/company.service";

export class AdminController extends RestfulController {
    constructor(private userService: UserService,
                private adminService: AdminService,
                private companyRoleService: CompanyRoleService,
                private companyService: CompanyService ) {
      super();
      this.router.get("/admin/companyList", this.handleRequest(this.getCompanyList))  //completed
      this.router.get("/admin/userList", this.handleRequest(this.getUserList)); //completed
      this.router.get("/admin/employeeApproval", this.handleRequest(this.employeeApproval)); //completed
      this.router.get("/admin/companyApproval", this.handleRequest(this.companyApproval))
      this.router.post("/admin/employeeApproval/Enable", this.handleRequest(this.approveUserAccount))//completed
      this.router.post("/admin/employeeApproval/WaitingList", this.handleRequest(this.getWaitingList))//completed


      this.router.post("/admin/updateUserAccount", this.handleRequest(this.updateUserAccount))//completed


      this.router.post("/admin/state", this.handleRequest(this.userState));
      this.router.post("/admin/add", this.handleRequest(this.addAdmin));
      this.router.post("/admin/delete", this.handleRequest(this.deleteAdmin));

      this.router.post("/admin/updateCompanyInfo", this.handleRequest(this.updateCompanyInfo));

      // this.router.post("/admin/banCompanyRole", this.handleRequest(this.banCompanyRole))
      // this.router.post("/admin/verifyCompanyRole", this.handleRequest(this.verifyCompanyRole))
      this.router.post("/admin/verifyEmployee", this.handleRequest(this.verifyEmployee))



    }

    is_admin = async  (req: express.Request) => {
        // let jwt = await requireJWTPayload(req);
        // let is_admin = jwt.payload.is_admin;
        // if(!is_admin){
        //     throw new HttpError("admin only", 400);
        // }
        // let id = jwt.payload.id
        // return id 
        return true
    }

    // get = async (req: express.Request) => {
    //   reutnawait knex.select('*').from('user').join('employee', 'employee.id', 'user.id')
    // };

    userState = async (req: express.Request) => {
        await this.is_admin(req)
        let { id, enable } = req.body;
        let result = await this.userService.updateUserState(id,enable)
        return result
    }

    getCompanyList = async  (req: express.Request) => {
      await this.is_admin(req)
      return await this.companyService.getCompanyList()
    };

    updateCompanyInfo = async  (req: express.Request) => {
      await this.is_admin(req)
      let { val, companyId, attribute } = req.body;

      return await this.companyService.updateCompanyInfoAttribute(val, companyId, attribute);
    };

    updateUserAccount = async  (req: express.Request) => {
      await this.is_admin(req)
      let { val, userId, attribute } = req.body;

      return await this.userService.updateUserAccountAttribute(val, userId, attribute);
    };

    approveUserAccount = async  (req: express.Request) => {
      await this.is_admin(req)
      let {id}=req.body

      return await this.userService.userApprove(id);
    };

    getUserList = async (req: express.Request) => {
        await this.is_admin(req)
        return this.userService.getUserList();
    };

    getWaitingList = async (req: express.Request) => {
      await this.is_admin(req)
      return this.userService.getWaitingApprove();
  };

    employeeApproval = async (req: express.Request) => {
      await this.is_admin(req)
      let {id}=req.body
      return this.userService.userApprove(id);
    };

    addAdmin = async  (req: express.Request) => {
      await this.is_admin(req)
      let {user_id} = req.body
      return await this.adminService.createAdmin(user_id)
    }

    deleteAdmin= async  (req: express.Request) => {
      await this.is_admin(req)
      let {user_id} = req.body
      return await this.adminService.deleteAdmin(user_id)
    }

    companyApproval = async  (req: express.Request) => {
      // await this.is_admin(req)
      return this.companyRoleService.getCompanyRoleListByAdmin()
    }

    // verifyCompanyRole = async  (req: express.Request) => {
    //   let {user_id , company_id} = req.body
    //   let admin_id = await this.is_admin(req)
    //   await this.userService.updateUserState(user_id, true)
    //   await this.companyRoleService.verifyCompanyRole(user_id,company_id, admin_id)
    // }

    // banCompanyRole = async  (req: express.Request) => {
    //   let {user_id , company_id} = req.body
    //   let admin_id = await this.is_admin(req)
    //   await this.companyRoleService.banCompanyRole(user_id,company_id, admin_id)
    // }
    verifyEmployee = async  (req: express.Request) => {
      let {user_id} = req.body
      await this.is_admin(req)
      await this.userService.updateUserState(user_id, true)
    }
  }