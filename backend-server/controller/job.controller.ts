import { RestfulController } from "./rest.controller";
import { JobFileService, JobService, JobTimeService } from "../service/job.service"
import express from "express";
import { requireCompanyJWT, requireJWTPayload } from "../jwt-helper";
import { HttpError } from "../http-error";
import { Job } from "../models";
import { Multer } from 'multer'
import fs from "fs"

export class JobController extends RestfulController {
  constructor(private jobService: JobService,
    private jobTimeService: JobTimeService,
    private jobFileService: JobFileService,
    private upload: Multer,) {
    super();
    this.router.post("/job/create", this.upload.array('file'), this.handleRequest(this.createJob));
    this.router.post("/job/time", this.handleRequest(this.postTime));
    


    this.router.get("/job/list", this.handleRequest(this.getJobList));
    this.router.get("/job/list/finish", this.handleRequest(this.getFinishJobDetail));
    this.router.get("/job/list/notyet", this.handleRequest(this.getNotYetJobDetail));
    this.router.get("/job/list/employee", this.handleRequest(this. getEmployeeJobDetail));

    this.router.delete("/job/file", this.handleRequest(this.deleteJobFile))
  }
  cleanup = async (req: express.Request) => {
    if (req.file) {
      // if (req.file.path){
      // }
      //S3 ?
      fs.unlink(req.file.path, (error) => {
        if (error) {
          throw new HttpError("Failed to cleanup attach:" + error, 404)
        }
      })
    }
  }


  createJob = async (req: express.Request) => {
    let {company_id , id:admin_id} = await requireCompanyJWT(req);

    let files: any = req.files
    let { name, salary, district_id, latlng, place, require_worker_num, other, start_date,end_date,daily_start_time, daily_end_time } = req.body


    let now = new Date(start_date);
    let end = new Date(end_date);
    // if (!start_time && !end_time) {
    //   throw new HttpError("job time unknow", 400);
    // }      
    type Slot = {
      start_time: string;
      end_time: string;
    };
    let slots: Slot[] = [];
    for (; now.getTime() < end.getTime();) {
      let start_time = now.toISOString();
      let [h, m] = daily_end_time.split(":");
      now.setHours(+h, +m, 0, 0);
      let end_time = now.toISOString();
      slots.push({ start_time, end_time });
      [h, m] = daily_start_time.split(":");
      now.setHours(24 + +h, +m, 0, 0);
    }
    if (slots.length > 0) {
      let last = slots[slots.length - 1];
      let date = new Date(last.end_time);
      let end = new Date(end_date);
      date.setHours(end.getHours(), end.getMinutes(), 0, 0);
      last.end_time = date.toISOString();
    }

    console.log(
      slots.map((slot) => {
        return {
          s: new Date(slot.start_time).toString(),
          e: new Date(slot.end_time).toString(),
        };
      })
    );



    let job: Job = { name, salary, district_id, company_id, admin_id, latlng, place, require_worker_num, other, start_date, end_date, daily_start_time, daily_end_time }
    let job_id = await this.jobService.createJob(job)
    // for (let i=0;i< start_time.length;i++)  {
    //   await this.jobTimeService.createJobTime(job_id, start_time[i], end_time[i])
    // }
    if (files) {
      for (let file of files) {
        let filename = file.key || file.filename
        await this.jobFileService.createJobFile(job_id, filename)
      }
    }
    return job_id
  }
  
  getJobList = async (req: express.Request, res: express.Response) => {
    let jwt = await requireJWTPayload(req);
    let company_id = jwt.payload.company_id;
    let list= await this.jobService.getJobList(company_id);
    return {list}

  }
  getFinishJobDetail = async (req: express.Request, res: express.Response) => {
    let jwt = await requireJWTPayload(req);
    let company_id = jwt.payload.company_id;
    let list= await this.jobService.getFinishJobList(company_id);
    return {list}
  }
  getNotYetJobDetail = async (req: express.Request, res: express.Response) => {
    let jwt = await requireJWTPayload(req);
    let company_id = jwt.payload.company_id;
    let list= await this.jobService.getNotYetJobList(company_id);
    return {list}
  }
  getEmployeeJobDetail = async (req: express.Request, res: express.Response) => {
    
    let list= await this.jobService.getEmployeeJobList();
    return {list}
  }

  postTime = async (req: express.Request, res: express.Response) => {
    let jwt = await requireJWTPayload(req);
    let company_id = jwt.payload.company_id;
    let {startDate, endDate}=req.body    
    console.log("jobcontroller.postTime",startDate, endDate)
    let list= await this.jobService.getSpecificJobList(company_id,startDate, endDate);
    return {list}
  }

  deleteJobFile = async (req: express.Request, res: express.Response) => {
    let filename = req.body.filename
    fs.unlink(filename, (error) => {
      if (error) {
        throw new HttpError("Failed to cleanup attach:" + error, 404)
      }
      return this.jobFileService.deleteJobFile(filename)
    })
  }
}


