import express from 'express'
import { HttpError } from '../http-error'
import fs from "fs"
import { s3 } from '../upload'
import { env } from '../env'
import '../express'

export class RestfulController {
  router = express.Router()

  handleRequest(
    fn: (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction,
    ) => Promise<any>,
  ) {
    return async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction,
    ) => {
      try {
        let json = await fn(req, res, next)
        res.json(json)
      } catch (error: any) {
        console.error(`API Error: ${req.method} ${req.url}, error:`, error)
        if (error instanceof HttpError) {
          res.status(error.status).json({ error: String(error.message) })
        } else {
          res.status(500).json({ error: String(error) })
        }
        if(req.files){
          if( Array.isArray(req.files)){
            req.files.forEach(file => deleteMulterFile(file))
          } else {
            Object.entries(req.files).forEach(([key, files]) => {
              files.forEach(file => deleteMulterFile(file))
            })
          }
        }
        if (req.file) {
          deleteMulterFile(req.file)
        }
       
      }
    }
  }
}


function deleteMulterFile( file : Express.Multer.File){
  if(file.key){
    s3.deleteObject({Key: file.key, Bucket: env.aws.s3Bucket}, (error) => {
      if (error) {
        console.log("Failed to cleanup s3 attach:", error)
      }
    })
}else{
  fs.unlink(file.path, (error) => {
    if (error) {
      console.log("Failed to cleanup file attach:", error)
    }
  })
}
}