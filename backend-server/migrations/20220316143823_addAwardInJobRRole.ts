import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    
        await knex.schema.alterTable("job_role", (table) => {
            table.string("award")

        });

}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("job_role", (table) => {
        table.dropColumn("award")
        
    });
}

