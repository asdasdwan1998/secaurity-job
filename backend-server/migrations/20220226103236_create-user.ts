import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    /* 1. Create users */
    if (!(await knex.schema.hasTable("users"))) {
        await knex.schema.createTable("users", (table) => {
            table.increments("id");
            table.string("email").unique();
            table.string("username",64).unique();
            table.string("password")
            table.string("mobile",20);
            table.string("icon");
            table.timestamps(false,true)
            table.boolean("enable").defaultTo(true);
        });
    }
    /* 2. district */
    if (!(await knex.schema.hasTable("district"))) {
        await knex.schema.createTable("district", (table) => {
            table.increments("id");
            table.string('name').notNullable()
        });
    }
    /* 3. Create employee */
    if (!(await knex.schema.hasTable("employee"))) {
        await knex.schema.createTable("employee", (table) => {
            table.integer("user_id").references('users.id').unique()
            table.string("fullname");
            table.enum('gender', ['f', 'm',])
            table.string("attach")
            table.time("start_time")
            table.time("end_time")
            table.integer('salary')
            table.integer('district_id').references('district.id')
            table.string('bank_account',50)
        });
    }
    /* 4. Create employee */
    if (!(await knex.schema.hasTable("admin"))) {
        await knex.schema.createTable("admin", (table) => {
            table.integer("user_id").references('users.id').unique()
            table.boolean("is_admin").defaultTo(false)
        });
    }

}


export async function down(knex: Knex): Promise<void> {

    await knex.schema.dropTable('admin')
    await knex.schema.dropTable('employee')
    await knex.schema.dropTable('district')
    await knex.schema.dropTable('users')
}

