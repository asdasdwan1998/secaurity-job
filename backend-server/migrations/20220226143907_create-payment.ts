import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    /* 1. Create payment */
    if (!(await knex.schema.hasTable("payment"))) {
        await knex.schema.createTable("payment", (table) => {
            table.increments("id")
            table.integer("admin_id").references('users.id').notNullable()
            table.timestamp("payment_time")
            table.timestamp("read_time")
        });
    }
    /* 2. Create salary */
    if (!(await knex.schema.hasTable("salary"))) {
        await knex.schema.createTable("salary", (table) => {
            table.increments("id")
            table.integer("job_role_id").references('job_role.id').notNullable()
            table.timestamp("start_time")
            table.timestamp("end_time")
            table.integer("score")
            table.integer("update_admin_id").references('users.id')
            table.timestamp("update_time")
            table.integer("payment_id").references('payment.id')
        });
    }
    /* 3. Create award_type */
    if (!(await knex.schema.hasTable("award_type"))) {
        await knex.schema.createTable("award_type", (table) => {
            table.increments("id")
            table.string("name",120).unique()
        });
    }
    /* 4. Create award */
    if (!(await knex.schema.hasTable("award"))) {
        await knex.schema.createTable("award", (table) => {
            table.increments("id")
            table.integer("employee_id").references('users.id').notNullable()
            table.integer("award_type_id").references('award_type.id').notNullable()
            table.integer("payment_id").references('payment.id')
            table.integer("amount").notNullable()
            table.string("desc")
        });
    }
}


export async function down(knex: Knex): Promise<void> {

    await knex.schema.dropTable('award')
    await knex.schema.dropTable('award_type')
    await knex.schema.dropTable('salary')
    await knex.schema.dropTable('payment')
}

