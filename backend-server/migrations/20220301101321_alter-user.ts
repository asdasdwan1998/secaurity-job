import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("users", (table) => {
    table.dropUnique(['username'])
    table.unique(['mobile'])
    table.renameColumn("username", "nickname");
    table.string('mobile',8).alter()
      table.timestamp("verify_expire_time");
      table.string("verify_code", 64);
      table.string("invite_code", 64).unique();
      table.dropColumn("password");
      table.integer("referrer_id").references("users.id");
    });

    await knex.schema.alterTable("job_role", (table) => {
      table.integer("score");
      table.integer("payment_id").references("payment.id");
    });
    
    await knex.schema.dropTableIfExists("salary");
}

export async function down(knex: Knex): Promise<void> {

    await knex.schema.createTable("salary", (table) => {
      table.increments("id");
      table.integer("job_role_id").references("job_role.id").notNullable();
      table.timestamp("start_time");
      table.timestamp("end_time");
      table.integer("score");
      table.integer("update_admin_id").references("users.id");
      table.timestamp("update_time");
      table.integer("payment_id").references("payment.id");

  })

    await knex.schema.table("job_role", (table) => {
      table.dropColumn("payment_id");
      table.dropColumn("score");
    });

    await knex.schema.alterTable("users", (table) => {
      table.dropColumn("referrer_id");
      table.string("password");
      table.dropColumn("invite_code");
      table.dropColumn("verify_code");
      table.dropColumn("verify_expire_time")
      table.string('mobile',20).alter()
    table.renameColumn("nickname", "username");
    table.unique(["username"])
    })
}
