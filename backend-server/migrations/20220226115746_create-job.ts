import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    /* 1. Create company */
    if (!(await knex.schema.hasTable("company"))) {
        await knex.schema.createTable("company", (table) => {
            table.increments("id")
            table.string("name").unique()
            table.integer("telephone")
            table.string("address")
            table.string("email")
            table.string('bank_account',50)
        });
    }
    /* 2. Create company role*/
    if (!(await knex.schema.hasTable("company_role"))) {
        await knex.schema.createTable("company_role", (table) => {
            table.integer("user_id").references('users.id').unique()
            table.integer("company_id").references('company.id').notNullable()
            table.integer("admin_id").references('users.id')
            table.timestamp('approve_admin_time')
            table.timestamp('reject_admin_time')
            table.timestamp('ban_role_time')
            table.timestamp('read_time')
        });
    }
    /* 3. Create job*/
    if (!(await knex.schema.hasTable("job"))) {
        await knex.schema.createTable("job", (table) => {
            table.increments("id")
            table.string("name").notNullable()
            table.integer('salary')
            table.integer('district_id').references('district.id')
            table.integer("company_id").references('company.id').notNullable()
            table.integer("admin_id").references('users.id')
            table.timestamp('start_work')
            table.timestamp('end_work')
            table.integer('attendance')
            table.point('latlng')
            table.string('place')
            table.boolean("enable").defaultTo(true);
        });
    }
    /* 4. Create job_file */
    if (!(await knex.schema.hasTable("job_file"))) {
        await knex.schema.createTable("job_file", (table) => {
            table.increments("id")
            table.string('mimetype',50).notNullable()
            table.string('filename').notNullable()
            table.integer("job_id").references('job.id').notNullable()
            table.string("desc")
        });
    }
    /* 5. Create job_role */
    if (!(await knex.schema.hasTable("job_role"))) {
        await knex.schema.createTable("job_role", (table) => {
            table.increments("id")
            table.integer("job_id").references('job.id').notNullable()
            table.integer("employee_id").references('users.id').notNullable()
            table.timestamp('confirm_time')
            table.timestamp('reject_time')
        });
    }
    /* 6. Create check_in */
    if (!(await knex.schema.hasTable("check_in"))) {
        await knex.schema.createTable("check_in", (table) => {
            table.increments("id")
            table.integer("job_role_id").references('job_role.id').notNullable()
            table.timestamp('time').notNullable()
            table.point('latlng').notNullable()
        });
    }
    /* 7. Create check_out */
    if (!(await knex.schema.hasTable("check_out"))) {
        await knex.schema.createTable("check_out", (table) => {
            table.increments("id")
            table.integer("check_in_id").references('check_in.id').notNullable()
            table.timestamp('time').notNullable()
            table.point('latlng').notNullable()
        });
    }
}


export async function down(knex: Knex): Promise<void> {

    await knex.schema.dropTable('check_out')
    await knex.schema.dropTable('check_in')
    await knex.schema.dropTable('job_role')
    await knex.schema.dropTable('job_file')
    await knex.schema.dropTable('job')
    await knex.schema.dropTable('company_role')
    await knex.schema.dropTable('company')

}

