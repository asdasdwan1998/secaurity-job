import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("job", (table) => {
        table.renameColumn('startDate', 'start_date')
        table.renameColumn('endDate','end_date')
        table.renameColumn('dailyStartTime','daily_start_time')
        table.renameColumn('dailyEndTime','daily_end_time')
    });

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("job", (table) => {
        table.renameColumn('start_date', 'startDate')
        table.renameColumn('end_date','endDate')
        table.renameColumn('daily_start_time','dailyStartTime')
        table.renameColumn('daily_end_time','dailyEndTime')
    });
}


