import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
     /* 1. Create message */
     if (!(await knex.schema.hasTable("message"))) {
        await knex.schema.createTable("message", (table) => {
            table.increments("id")
            table.integer("sender_id").references('users.id').notNullable()
            table.integer("receiver_id").references('users.id')
            table.integer("job_id").references('job.id')
            table.string("content")
            table.timestamp("send_time")
        });
    }
     /* 2. Create read_message */
     if (!(await knex.schema.hasTable("read_message"))) {
        await knex.schema.createTable("read_message", (table) => {
            table.integer("user_id").references('users.id').notNullable()
            table.integer("message_id").references('message.id').notNullable()
            table.timestamp("read_time")
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('read_message')
    await knex.schema.dropTable('message')
}

