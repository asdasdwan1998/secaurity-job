import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
        await knex.schema.alterTable("company", (table) => {
            table.string("principal")
        })

        await knex.schema.alterTable("job", (table) => {
            table.integer('require_worker_num')
            table.string('other')

        });

}

export async function down(knex: Knex): Promise<void> {
        await knex.schema.alterTable("company", (table) => {
            table.dropColumn("principal")
    })
        await knex.schema.alterTable("job", (table) => {
            table.dropColumn('require_worker_num')
            table.dropColumn('other')

})
}

