import { Server as SocketIO, Socket } from "socket.io";
import { userService } from "./services";

export let io: SocketIO;

export function setIO(socketIO: SocketIO) {
  io = socketIO;
  io.on("connection", (socket) => {
    // console.log("socket.io client connected:", socket.id);
    socket.on("login", async (token: string) => {
      if (!token) {
        return;
      }
      let jwt = await userService.decodeToken(token);
      let id = jwt.payload.id;
      userService.markUserOnline(id);
      socket.on("disconnect", () => {
        console.log("close", id);
        userService.unUserOnline(id);
      });
    });
    socket.on("join", (channel) => {
        socket.join(channel);
    });
  });
}
