import { knex } from './db'
import { ImageService } from './image-service'
import { EmailService } from './service/email.service'
import { SmsService } from './service/sms.service'
import { UserService } from './service/user.service'

export let imageService = new ImageService(knex)

let smsService = new SmsService()
let emailService = new EmailService()
export let userService = new UserService(knex,emailService,smsService)
