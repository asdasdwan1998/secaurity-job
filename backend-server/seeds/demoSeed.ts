import { Knex } from "knex";
import { CompanyService } from "../service/company.service";
import { UserService } from "../service/user.service";
import { DistrictService } from "../service/district.service";
import { EmailService } from "../service/email.service";
import { SmsService } from "../service/sms.service";
import { AdminService } from "../service/admin.service";
import { JobService } from "../service/job.service";
import { JobRoleService } from "../service/jobRole.service";
import { MessageService } from "../service/message.service";
import { EmployeeService } from "../service/employee.service";

export async function seed(knex: Knex): Promise<void> {
  let companyService = new CompanyService(knex);
  let emailService = new EmailService();
  let smsService = new SmsService();
  let userService = new UserService(knex, emailService, smsService);
  let districtService = new DistrictService(knex);
  let adminService = new AdminService(knex);
  let jobService = new JobService(knex);
  let jobRoleService = new JobRoleService(knex);
  let messageService = new MessageService(knex);
  let employeeService = new EmployeeService(knex);

  let DonaldEmail = "donald@example.net";
  let DonaldId = await userService.getUserIdByContact(DonaldEmail);
  let company_id =
    (await companyService.getCompanyIdByName("tecky")) ||
    (await companyService.createCompany(
      {
        name: "tecky",
        telephone: "64648989",
        address: "HK,TW",
        email: "tecky@gmail.com",
        bank_account: "465-789436589785",
        principal: "ALEX",
      },
      DonaldId
    ));

  let admin_email = "admin@example.net";
  let admin_id = await userService.getUserIdByContact(admin_email);
  await adminService.createAdmin(admin_id);

  await companyService.approveCompany({
    admin_id,
    user_id: DonaldId,
    company_id,
  });

  let district = [
    "中西區",
    "灣仔",
    "東區",
    "南區",
    "油尖旺",
    "深水埗",
    "九龍城",
    "黃大仙",
    "觀塘",
    "葵青",
    "荃灣",
    "屯門",
    "元朗",
    "北區",
    "大埔",
    "沙田",
    "西貢",
    "離島",
  ];

  for (let name of district) {
    let id = await districtService.createDistrict(name);
    console.log("district", id);
  }

  let job_id =
    (await jobService.getJobIdByName("動漫節")) ||
    (await jobService.createJob({
      admin_id: DonaldId,
      district_id: 1,
      company_id: company_id,
      name: "動漫節",
      salary: 2000,
      latlng: "(22.374477,114.1082506)",
      place: "梨木樹",
      require_worker_num: 5,
      other: "testing",
      start_date: "2022-03-10 10:00:00+08",
      end_date: "2022-03-12 20:00:00+08",
      daily_start_time: "10:00:00",
      daily_end_time: "20:00:00",
    }));
  let job_id2 =
    (await jobService.getJobIdByName("")) ||
    (await jobService.createJob({
      admin_id: DonaldId,
      district_id: 1,
      company_id: 1,
      name: "麗港城臨時看更",
      salary: 5000,
      latlng: "(22.374477,114.1082506)",
      place: "麗港城",
      require_worker_num: 5,
      other: "testing",
      start_date: "2022-03-10 10:00:00+08",
      end_date: "2022-03-20 20:00:00+08",
      daily_start_time: "10:00:00",
      daily_end_time: "20:00:00",
    }));
  (await jobService.getJobIdByName("")) ||
    (await jobService.createJob({
      admin_id: 1,
      district_id: 1,
      company_id: 1,
      name: "珀麗灣臨時看更",
      salary: 3000,
      latlng: "(22.374477,114.1082506)",
      place: "珀麗灣",
      require_worker_num: 5,
      other: "testing",
      start_date: "2022-03-19 10:00:00+08",
      end_date: "2022-03-13 20:00:00+08",
      daily_start_time: "10:00:00",
      daily_end_time: "20:00:00",
    }));
  (await jobService.getJobIdByName("")) ||
    (await jobService.createJob({
      admin_id: 1,
      district_id: 1,
      company_id: 1,
      name: "ICA臨時看更",
      salary: 5000,
      latlng: "(22.374477,114.1082506)",
      place: "ICA",
      require_worker_num: 5,
      other: "testing",
      start_date: "2022-03-20 10:00:00+08",
      end_date: "2022-03-30- 20:00:00+08",
      daily_start_time: "10:00:00",
      daily_end_time: "20:00:00",
    }));
  (await jobService.getJobIdByName("")) ||
    (await jobService.createJob({
      admin_id: 1,
      district_id: 1,
      company_id: 1,
      name: "Freddy Fazbear's Pizza midnight guard",
      salary: 10000,
      latlng: "(22.500465,114.1082506)",
      place: "Freddy Fazbear's Pizza",
      require_worker_num: 1,
      other: "testing",
      start_date: "2022-03-15 22:00:00+08",
      end_date: "2022-03-20 05:00:00+08",
      daily_start_time: "23:00:00",
      daily_end_time: "05:00:00",
    }));

  let employee1Id = await userService.getUserIdByContact("alice@example.net");
  await employeeService.createEmployee(employee1Id, {
    bank_account: "123-456789456",
    fullname: "alice wong",
  });

  let employee2Id = await userService.getUserIdByContact("alex@example.net");
  await employeeService.createEmployee(employee2Id, {
    bank_account: "123-555789456",
    fullname: "alex lam",
  });

  let employee3Id = await userService.getUserIdByContact("bob@example.net");
  await employeeService.createEmployee(employee3Id, {
    bank_account: "789-456789456",
    fullname: "bob wong",
  });

  let employee4Id = await userService.getUserIdByContact("charlie@example.net");
  await employeeService.createEmployee(employee4Id, {
    bank_account: "456-456789456",
    fullname: "charlie wong",
  });

  let amenEmail = "amen@example.net";
  let amenId = await userService.getUserIdByContact(amenEmail);

  let testEmail = "test@example.net";
  let testId = await userService.getUserIdByContact(testEmail);

  let message = [
    { sender_id: amenId, receiver_id: DonaldId, content: "hi Donald" },
    {
      sender_id: amenId,
      receiver_id: admin_id,
      content: "hi admin, please help me to debug with approve my account",
    },
    { sender_id: amenId, content: "動漫節幾時開始", job_id: job_id },
    {
      sender_id: testId,
      receiver_id: amenId,
      content: "hi amen, l am testing something",
    },
    {
      sender_id: DonaldId,
      receiver_id: amenId,
      content: "hi amen, l am Donald",
    },
    { sender_id: amenId, receiver_id: DonaldId, content: "nice to meet you" },
    {
      sender_id: DonaldId,
      content: "不知道，大約8月尾 或者 今年又取消吧",
      job_id: job_id,
    },
    { sender_id: amenId, content: "真糟糕", job_id: job_id },
  ];
  // let message_row = await messageService.getMessage(amenId)

  for (let row of message) {
    setTimeout(async () => {
      console.log("row: ", row);
      let message_id = await messageService.sendMessage(row);
      console.log(message_id);
    }, 5000);
  }

  let amen_job_role_id = await jobRoleService.createJobRole(job_id, amenId);
  let Donald_job_role_id = await jobRoleService.createJobRole(job_id, DonaldId);
  let test_job_role_id = await jobRoleService.createJobRole(job_id2, amenId);
  let Donald_job_role_id2 = await jobRoleService.createJobRole(
    job_id2,
    DonaldId
  );

  await jobRoleService.confirmJobRoleByID(amen_job_role_id);
  await jobRoleService.confirmJobRoleByID(Donald_job_role_id);
  await jobRoleService.confirmJobRoleByID(test_job_role_id);
  await jobRoleService.confirmJobRoleByID(Donald_job_role_id2);

  await userService.updateUser(amenId, { nickname: "amenEX" });
  await userService.updateUser(DonaldId, { nickname: "Donald" });
  await userService.updateUser(testId, { nickname: "test" });
  await userService.updateUser(admin_id, { nickname: "admin" });
}
