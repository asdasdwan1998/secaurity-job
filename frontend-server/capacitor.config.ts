import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'xyz.propertyhk.app.employee',
  appName: 'Security Guard Job Platform',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
