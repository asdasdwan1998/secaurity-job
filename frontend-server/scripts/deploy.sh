set -e

# remove "--delete" if you're using lazy loading to avoid breaking clients running old version
aws s3 sync --delete ./build s3://guard-frontend
aws cloudfront create-invalidation --distribution-id E1SB5MJRALDNNF --paths '/*'
