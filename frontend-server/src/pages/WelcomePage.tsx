import { IonButton, IonCard, IonCardContent, IonContent, IonHeader, IonPage } from '@ionic/react';
// import { logoApple, logoFacebook, logoGoogle, logoTwitter } from 'ionicons/icons';
import { appName } from '../config';
import { routes } from '../routes';
import './WelcomePage.css'

// const IconButton= (props: {
//   class?: string
//   icon: string
// }) => {
//   return (
//     <IonButton className={props.class}>
//     <IonIcon icon={props.icon}></IonIcon>
//     </IonButton>
//   )
// }

// export let icons = [{ class: "btn-fb" , icon: logoFacebook },{ class: "btn-tw" , icon: logoTwitter },{ class: "btn-go" , icon: logoGoogle },{ class: "btn-ap" , icon: logoApple }]



const WelcomePage: React.FC = () => {

  return (
    <IonPage>
      <IonHeader className="header-half" class="ion-no-border">
        <div className="logo">
            <img src="assets/image/default.png" alt='logo'/>
          </div>
          {/* <h2>{appName}</h2> */}
      </IonHeader>
      <IonCard class="ion-padding welcome-ioncard">
            <IonCardContent>
              <h1>Welcome to {appName}.</h1>
              <p></p>
              <IonButton expand='block' routerLink={routes.login}>開始使用</IonButton>
        
              {/* <div className="line_text">
                <span className="lines"></span>
                <span className="text">Or Connect using</span>
              </div>

                <IonButtons className="socials">
                {icons.map(icon => (<IconButton class={icon.class} icon={icon.icon}></IconButton>))}
                </IonButtons> */}

            </IonCardContent>
          </IonCard>
      <IonContent fullscreen style={{"--background": '#e0dfe6'}}>
          
      </IonContent>
    </IonPage>
  );
};

export default WelcomePage;
