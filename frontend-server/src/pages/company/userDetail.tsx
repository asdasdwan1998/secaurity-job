import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonInput, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useState } from 'react';
import { useGet } from "../../hooks/use-get"
import { usePost } from '../../hooks/use-post';
import { routes } from '../../routes';



const UserDetail: React.FC = () => {
  const [nickname, setNickname] = useState<string>("");
  const [mobile, setMobile] = useState<string>("");
  const [Email, setEmail] = useState<string>("");
  const update = usePost("/update/user")
  const profile = useGet('/user/profile', { error: "", id: "", mobile: "", email: "", nickname: "" })

  async function submitEdit() {

    update.post(
      {
        nickname,
        mobile,
        Email
      },
      json => {
        update.showToast('posted successfully')

      })
    profile.refresh()

  }
  return (
    <IonPage>
      <IonHeader>

        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.company.home}></IonBackButton>
          </IonButtons>
          <IonTitle>User Detail</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">User Detail</IonTitle>
          </IonToolbar>
        </IonHeader>
        {profile.render((profile) =>
          <div>


            <h1>名稱:{profile.nickname}</h1>
            <IonLabel >編輯名稱:<IonInput value={nickname}style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} type='text' name='nickname' onIonChange={e => setNickname(e.detail.value!)}></IonInput>
            </IonLabel>
            <h1>ID:{profile.id}</h1><br></br>
            <h1>聯絡方法</h1>
            <h3>聯絡電話:{profile.mobile}</h3>
            <IonLabel >編輯電話:<IonInput value={mobile}style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} type='text' name='mobile' onIonChange={e => setMobile(e.detail.value!)}></IonInput>
            </IonLabel>
            <h3>E-mail:{profile.email}</h3>
            <IonLabel >編輯電郵:<IonInput value={Email} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} type='text' name='email' onIonChange={e => setEmail(e.detail.value!)}></IonInput>
            </IonLabel>

            <IonButton onClick={submitEdit}>更新個人資料</IonButton>

          </div>)}


      </IonContent>
    </IonPage>
  );
};

export default UserDetail;


