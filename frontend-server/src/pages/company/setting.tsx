import { IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar, useIonToast } from '@ionic/react';
import { useDispatch } from 'react-redux';
import { logoutThunk } from '../../redux/auth/thunk';

const CompanySetting: React.FC = () => {
  const dispatch = useDispatch();
  async function logout(){
    dispatch(
      logoutThunk(showSuccess)
    );
    }
    async function showSuccess(){
      present("logout sucessfully", 5000);
    }
    const [present] = useIonToast();
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Setting</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Setting</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonButton expand='block'  onClick={logout}>Log out</IonButton>
      </IonContent>
    </IonPage>
  );
};

export default CompanySetting;