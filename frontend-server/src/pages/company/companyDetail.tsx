import { IonBackButton, IonButtons, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useGet } from '../../hooks/use-get';
import { routes } from '../../routes';



  
   


const CompanyDetail: React.FC = () => {

  const profile=useGet('/company/profile',{error:"",principal:"",name:"",telephone:"",address:"",email:"",bank_account:""})
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.company.home}></IonBackButton>
          </IonButtons>
            <IonTitle>Company Detail</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className='ion-padding'>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Company Detail</IonTitle>
            </IonToolbar>
          </IonHeader>
          {profile.render((profile) =>
           <div style={{margin:'20px'}}>

        <h1>公司名稱:{profile.name}</h1>
        <h1>公司負責人:{profile.principal}</h1>
        <h1>銀行帳戶:{profile.bank_account}</h1>
        <h1>公司聯絡方法:</h1><br></br>
        <h3>地址:{profile.address}</h3>
        <h3>聯絡電話:{profile.telephone}</h3>
        <h3>E-mail:{profile.email}</h3>

    </div>)}

        </IonContent>
      </IonPage>
    );
  };
  
  export default CompanyDetail;