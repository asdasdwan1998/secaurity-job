import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonPage,
  IonThumbnail,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import { useState } from "react";
import { usePost } from "../../hooks/use-post";
import { routes } from "../../routes";
import { KB } from "@beenotung/tslib/size";
import {
  compressMobilePhoto,
  dataURItoBlob,
  resizeBase64WithRatio,
} from "@beenotung/tslib/image";
import { selectImage } from "@beenotung/tslib/file";

const CreateCompany: React.FC = () => {
  const [nickname, setNickname] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [principal, setPrincipal] = useState<string>("");
  const [telephone, setTelephone] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [bank_account, setBankAccount] = useState<string>("");
  const [address, setAddress] = useState<string>("");
  const [icon_file, setIcon_file] = useState<File>();
  const [icon, setIcon] = useState("");
  const router = useIonRouter();

  const submit = usePost("/company");

  async function submitEdit() {
    submit.postFile(
      {
        nickname,
        name,
        principal,
        telephone,
        email,
        bank_account,
        address,
        icon_file,
      },
      (json) => {
        submit.showToast("create successfully");
        router.push(routes.company.home);
      }
    );
  }

  const addPhoto = async () => {
    let files = await selectImage({ multiple: false, accept: "image/*" });
    for (let file of files) {
      let dataUrl = await compressMobilePhoto({
        image: file,
        maximumSize: 300 * KB,
      });
      dataUrl = await resizeBase64WithRatio(
        dataUrl,
        { width: 128, height: 128 },
        "with_in"
      );
      let blob = dataURItoBlob(dataUrl);
      file = new File([blob], file.name, {
        type: blob.type,
        lastModified: file.lastModified,
      });
      setIcon(dataUrl);
      setIcon_file(file);
    }
  };



  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.register}></IonBackButton>
          </IonButtons>
          <IonTitle>公司資料</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
      <IonList lines="full">
        <IonListHeader><h3>個人資料</h3></IonListHeader>
        <div className="icon">
                <IonThumbnail>
                  <img
                    alt="icon"
                    src={icon || "./assets/image/companylogo2nd.png"}
                    onError={(e) => {
                      e.currentTarget.src = "./assets/image/companylogo2nd.png"}}
                  />
                </IonThumbnail>
              </div>
        <IonItem>
            <IonLabel>帳號稱呼:</IonLabel>
            <IonInput
              value={nickname}
              type="text"
              name="nickname"
              onIonChange={(e) => setNickname(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
              <IonButton onClick={addPhoto}>上傳頭像</IonButton>
          </IonItem>
        </IonList>
        <br></br>
        <br></br>
        <IonList lines="full">
        <IonListHeader><h3>公司資料</h3></IonListHeader>
          <IonItem>
            <IonLabel>公司名稱:</IonLabel>
            <IonInput
              value={name}
              type="text"
              name="nickname"
              onIonChange={(e) => setName(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel>公司負責人:</IonLabel>
            <IonInput
              value={principal}
              type="text"
              name="nickname"
              onIonChange={(e) => setPrincipal(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel>公司電話:</IonLabel>
            <IonInput
              value={telephone}
              type="text"
              name="mobile"
              onIonChange={(e) => setTelephone(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel>公司電郵:</IonLabel>
            <IonInput
              value={email}
              type="text"
              name="email"
              onIonChange={(e) => setEmail(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel>公司地址:</IonLabel>
            <IonInput
              value={address}
              type="text"
              name="email"
              onIonChange={(e) => setAddress(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel>付款帳戶:</IonLabel>
            <IonInput
              value={bank_account}
              type="text"
              name="email"
              onIonChange={(e) => setBankAccount(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonButton onClick={submitEdit}>更新個人資料</IonButton>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default CreateCompany;
