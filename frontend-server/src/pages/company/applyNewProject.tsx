import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonInput, IonLabel, IonPage, IonSelect, IonSelectOption, IonTitle, IonToolbar, useIonAlert } from '@ionic/react';
import './applyNewproject.module.css'
import { useEffect, useState } from "react"
import { Map, Marker, ZoomControl } from "pigeon-maps" //https://pigeon-maps.js.org/
import { Geolocation } from '@capacitor/geolocation';
import { FileUploader } from "react-drag-drop-files";
import { usePost } from "../../hooks/use-post";
import { useGet } from '../../hooks/use-get';
import { routes } from '../../routes';

import { useHistory } from 'react-router';
import React from 'react';

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

const printCurrentPosition = async () => {
  const coordinates = await Geolocation.getCurrentPosition();
  console.log('Current position:', coordinates);
  return coordinates
};

// type ProjectDetail = { projectName: string, currLocation: string, district: string, place: string, startTime: string, endTime: string, humanCount: string, salary: string, other: string,dailyStartTime:string,dailyEndTime:string}

const NewProject: React.FC = () => {
  const history = useHistory();
  const projectDetail = usePost('/job/create')
  const list = useGet("/district",{error:"",list:[]as
  {
    id:number,
    name:string
  }[]
})

  async function submitProject() {
    if (salary.includes("-")){
      present("薪金不能為負數", [{ text: "Close" }]);
      return;
    }
    if (salary==="0"){
      present("薪金不能為0", [{ text: "Close" }]);
      return;
    }
    if (salary==="0"){
      present("薪金不能為0", [{ text: "Close" }]);
      return;
    }
    if (humanCount==="0"){
      present("所需人數不能為0", [{ text: "Close" }]);
      return;
    }
    if (humanCount.includes("-")){
      present("所需人數不能為負數", [{ text: "Close" }]);
      return;
    }
    if (start_date ===("")){
      present("欠缺start_date", [{ text: "Close" }]);
      return;
    }
    if (end_date ===("")){
      present("欠缺部分必需資料", [{ text: "Close" }]);
      return;
    }
    if (projectName ===("")){
      present("欠缺end_date", [{ text: "Close" }]);
      return;
    }
    if (place ===("")){
      present("欠缺place ", [{ text: "Close" }]);
      return;
    }

    projectDetail.post(
      { latlng:"("+currLocation+")",
       name:projectName,
       district_id:district,
       place,
       start_date, 
       end_date, 
       daily_start_time,
       daily_end_time,
       require_worker_num:humanCount, 
       salary, 
       other },
      json => {
        projectDetail.showToast('posted successfully')
        history.push("/company/projectManage")
        window.location.reload()
        

      })
  }



  // const status = "not-complete"
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  const [currLocation, setCurrLocation] = useState<[number, number]>([0, 0]); // your gps location
  const [center, setCenter] = useState<[number, number]>([22.302711, 114.177216]); // general center location, where [22.302711,114.177216] in HK location

  const [zoom, setZoom] = useState<number>(11);
  const [projectName, setProjectName] = useState<string>("");
  const [district, setDistrict] = useState<string>("");
  const [place, setPlace] = useState<string>("");
  const [start_date, setStartDate] = useState<string>("");
  const [end_date, setEndDate] = useState<string>("");
  const [daily_start_time, setDailyStartTime] = useState<string>("");
  const [daily_end_time, setDailyEndTime] = useState<string>("");
  const [humanCount, setHumanCount] = useState<string>("");
  const [salary, setSalary] = useState<string>("");
  const [other, setOther] = useState<string>("")
  const [appendix, setAppendix] = useState<any>(null)
  const [present] = useIonAlert();

  useEffect(() => { // resize handler

    const handleResize = () => setWindowDimensions(getWindowDimensions());
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);


  useEffect(() => { // get current location ONE time
    (async () => {
      let result = await printCurrentPosition() as any;
      setCurrLocation([result.coords.latitude, result.coords.longitude])
    })()
  }, [])

  //goto current location
  function gotoCurrentLocation() {
    setZoom(16);
    setCenter(currLocation);
  }
  
  function ClickHandler(obj: any) {
    console.log(obj);
    setCurrLocation(obj.latLng)
  }


  return (
    <IonPage class="newProjectPage">
      <IonHeader>
        <IonToolbar>
        <IonButtons slot="start">
             <IonBackButton defaultHref={routes.company.home}></IonBackButton>

          </IonButtons>
          <IonTitle>Setting</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Setting</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonLabel >
          項目名稱:
          <IonInput value={projectName} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} type="text" name='Project Name' placeholder="Enter Name" onIonChange={e => setProjectName(e.detail.value!)}>

          </IonInput>
        </IonLabel><br></br>
        <IonLabel >
          工作地區:   
          <IonSelect value={district} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}}className="projectinput"  name='district' placeholder="Enter area" onIonChange={e => setDistrict(e.detail.value!)}>

          {Array.isArray(list.state)  && list.state.map((v:any)=>(
            <IonSelectOption value={v.id}>{v.name}</IonSelectOption>
          ))}
         
          </IonSelect>
        </IonLabel><br></br>
        <IonLabel >
          工作地點:
          <IonInput value={place} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" type="text" name='Project Name' placeholder="Enter address" onIonChange={e => setPlace(e.detail.value!)}>
          </IonInput>
        </IonLabel><br></br>

        <IonLabel >
          工作坐標:
          <IonInput type="text" style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" name='Project Name' value={currLocation.join(",")}></IonInput>
        </IonLabel><br></br>
        <>

          <div style={{ textAlign: "left" }}>
            <IonButton color="dark" onClick={() => gotoCurrentLocation()}>Go Current location</IonButton>
          </div>

          <Map height={windowDimensions.height * 0.30}
            defaultCenter={[22.302711, 114.177216]}
            defaultZoom={zoom}
            zoom={zoom}
            center={center}
            onClick={ClickHandler}
            onBoundsChanged={({ center, zoom }) => {
              setCenter(center)
              setZoom(zoom)
            }}
          >
            <ZoomControl />


            <Marker width={50} anchor={currLocation} />
          </Map>
        </>
        <IonLabel >
          開始時間:
          <IonInput value={start_date} style={{border: "1px solid rgb(101, 101, 101)" ,borderRadius:"10px"}} className="projectinput" type='datetime-local' name='date' onIonChange={e => setStartDate(e.detail.value!)}>
          </IonInput>
        </IonLabel><br></br>
        <IonLabel >
          結束時間:
          <IonInput value={end_date} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" type='datetime-local' name='date' onIonChange={e => setEndDate(e.detail.value!)}></IonInput>
          
        </IonLabel><br></br>
        <IonLabel >
        每天上班時間(如有需要):
        <IonInput value={daily_start_time} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" type='time' name='date' onIonChange={e => setDailyStartTime(e.detail.value!)}></IonInput>至
        <IonInput value={daily_end_time} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" type='time' name='date' onIonChange={e => setDailyEndTime(e.detail.value!)}></IonInput>



        </IonLabel>
        <IonLabel >
          所需人數:
          <IonInput value={humanCount} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" type='number' name='number' placeholder="Enter Number" onIonChange={e => setHumanCount(e.detail.value!)}></IonInput>
        </IonLabel><br></br>
        <IonLabel >
          薪金:
          <IonInput value={salary} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" type='number' name='number' placeholder="Enter Number" onIonChange={e => setSalary(e.detail.value!)}
          ></IonInput>
        </IonLabel><br></br>
        <IonLabel >
          備註(如有需要):
          <IonInput value={other} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} className="projectinput" type='text' name='text' onIonChange={e => setOther(e.detail.value!)}></IonInput>
        </IonLabel><br></br>
        <IonLabel >
          附件(如有需要):
        </IonLabel>
        <FileUploader handleChange={(file: any) => setAppendix(file)} name="file" types={["PDF"]} value={appendix} />

        <IonButton onClick={submitProject}>submit</IonButton>
        {projectDetail.renderError()}



      </IonContent>
    </IonPage>
  );
};

export default NewProject;