import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonButton, IonImg } from '@ionic/react';
import { routes } from '../../routes';
import'./homepage.css'

// const companyLogo=(
// <div className='ion-text-center'><img src="./companylogo.png"  ></img></div>
// )
const CompanyHomePage: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Home
          </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Home</IonTitle>
          </IonToolbar>
        </IonHeader>
     
        <IonImg src="assets/image/companylogo2nd.png" style={{width: 80, height: 80,margin:'auto',marginTop:20}}/>
        <IonButton className='CompanyButton' expand='block'  routerLink={routes.company.userDetail}>帳戶資料</IonButton>
        <IonButton className='CompanyButton' expand='block'  routerLink={routes.company.CompanyDetail}>公司基本檔案管理</IonButton>
        <IonButton className='CompanyButton' expand='block' routerLink={routes.company.projectManage}>項目管理</IonButton>
        <IonButton className='CompanyButton' expand='block' routerLink={routes.company.timetable}>時間表管理</IonButton>
      </IonContent>
    </IonPage>
  );
};

export default CompanyHomePage;
