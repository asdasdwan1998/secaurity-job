import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonPage, IonSegment, IonSegmentButton, IonTitle, IonToolbar } from '@ionic/react';
import { routes } from '../../routes';
import './projectManage.css'
import { useGet } from "../../hooks/use-get"
import { useState } from 'react';
import { formatDateTime } from '../../format';
import { add } from "ionicons/icons";

const ProjectManage: React.FC = () => {

  const completeState = useState<'all' | 'completed' | 'not-complete'>('not-complete')

  const list = useGet(
    completeState[0] === 'all'
      ? '/job/list'
      : completeState[0] === 'completed'
        ? '/job/list/finish'
        : '/job/list/notyet',
    {
      error: "", list: [] as
        {
          id: number,
          name: string,
          salary: string,
          place: string,
          require_worker_num: number,
          start_date: string,
          end_date: string
          daily_start_time: string,
          daily_end_time: string
        }[]
    }
  )

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.company.home}></IonBackButton>

          </IonButtons>
          <IonTitle>ProjectManage</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">projectManage</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonSegment className='project select'
        mode="ios"
        value={completeState[0]}
        onIonChange={e=>completeState[1](e.detail.value as any)}>
          <IonSegmentButton value='all' >全部</IonSegmentButton>
          <IonSegmentButton value='not-complete'>未完成</IonSegmentButton>
          <IonSegmentButton value='completed'>已完成</IonSegmentButton>
        </IonSegment>
     

        {list.render((json) => json.list?.map(job =>
          <div className='project-list ' key={job.id} >
            <h6>項目名稱:{job.name}</h6>
            <h6>工作地點:{job.place}</h6>
            <h6>工作日期:{formatDateTime(job.start_date)} 至 <br></br>{formatDateTime(job.end_date)}</h6>
            <h6>工作時間:{job.daily_start_time} 至 {job.daily_end_time}</h6>
            <h6>所需人數:{job.require_worker_num}</h6>
            <h6>每位薪金:{job.salary}</h6>
            
            <IonButton className='CompanyButton' expand='block' routerLink={routes.company.jobEmployeeList(job.id)}>Member</IonButton>
          </div>
        ))}

        <IonButton expand="block" routerLink={routes.company.NewProject}><IonIcon icon={add} size="medium" style={{fontSize:"30px"}} ></IonIcon>New Project
</IonButton>
      </IonContent>
    </IonPage>
  );
};

export default ProjectManage;