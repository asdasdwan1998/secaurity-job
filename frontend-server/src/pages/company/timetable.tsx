import React, { useState } from 'react'
import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonInput, IonLabel, IonPage, IonTitle, IonToolbar, useIonAlert } from '@ionic/react';
import { routes } from '../../routes';
import { formatDateTime } from '../../format';
import { post } from '../../api';

const TimeTable: React.FC = () => {
  const [StartTime, setStartTime] = useState<string>("");
  const [EndTime, setEndTime] = useState<string>("");
  const [list, setList] = useState<Job[]>([])
  const [present] = useIonAlert();

  async function submitTime() {
  // const time = usePost("/job/time")
  console.log(StartTime)
  console.log(EndTime)
  if (!StartTime){
    present("缺少開始時間", [{ text: "Close" }]);
    return;
  }
  if (!EndTime){
    present("缺少結束時間", [{ text: "Close" }]);
    return;
  }

  let donald = await post("/job/time", 
    {
      startDate: StartTime,
      endDate: EndTime
    })
    setList(donald.list)
    console.log('donald :', donald.list);
    console.log('list: ', list);
    
  }

 type Job={id: number,
          name: string,
          salary: string,
          place: string,
          require_worker_num: number,
          start_date: string,
          end_date: string,
          daily_start_time: string,
          daily_end_time: string}

  // const list = useGet(
  //   '/job/list/specific'
  //   ,
  //   {
  //     error: "", list: [] as
  //       {
  //         id: number,
  //         name: string,
  //         salary: string,
  //         place: string,
  //         require_worker_num: number,
  //         start_date: string,
  //         end_date: string,
  //         daily_start_time: string,
  //         daily_end_time: string
  //       }[]
  //   }

  // )


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.company.home}></IonBackButton>
          </IonButtons>
          <IonTitle>Time manage</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Time manage</IonTitle>

          </IonToolbar>
        </IonHeader>

        <IonInput type="date" value={StartTime} name='Project Name' placeholder="Enter Name" onIonChange={e => setStartTime(e.detail.value!)}>
          <IonLabel>Start-from</IonLabel>
        </IonInput>
        <IonInput type="date" value={EndTime} name='Project Name' placeholder="Enter Name" onIonChange={e => setEndTime(e.detail.value!)}>
          <IonLabel>End</IonLabel>
        </IonInput>
        <IonButton onClick={submitTime}>submit</IonButton>

        {list.map((job:Job)=>
        <div className='project-list'>
           <h6>項目名稱:{job.name}</h6>
            <h6>工作地點:{job.place}</h6>
            <h6>工作日期:{formatDateTime(job.start_date)} 至 <br></br>{formatDateTime(job.end_date)}</h6>
            <h6>工作時間:{job.daily_start_time} 至 {job.daily_end_time}</h6>
            <h6>所需人數:{job.require_worker_num}</h6>
            <h6>每位薪金:{job.salary}</h6>
        </div>
        )}




      </IonContent>

    </IonPage>
  );
};

export default TimeTable