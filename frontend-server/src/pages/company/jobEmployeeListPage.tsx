import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonInput, IonLabel, IonPage, IonSelect, IonSelectOption, IonTitle, IonToolbar } from '@ionic/react';
import { personCircle } from 'ionicons/icons';
import { useState } from 'react';
import { useParams } from 'react-router';
import { useGet } from '../../hooks/use-get';
import { usePost } from '../../hooks/use-post';
import { routes } from '../../routes';

import "./jobEmployeeListPage.css"
const JobEmployeeList: React.FC = () => {
  const [specialAward, setSpecialAward] = useState<string>("");
  const [score, setScore] = useState<number>(5);

  let { job_id } = useParams<any>()
  const list = useGet(`/job/${job_id}/employee-list`
    ,
    {
      error: "", list: [] as
        {
          id: number,
          employee_id: number,
          nickname: string,
          fullname: string,
          score:number
        }[]
    }
  )
  const specialAwardPost = usePost("/job/company/specialAward")
  const scorePost = usePost("/job/role/score")


  async function awards(id: number, specialAward: string) {
    console.log(id)

    specialAwardPost.post(
      {
        id, specialAward
      },
      json => {
        specialAwardPost.showToast('post award')
      })

  }
  
  async function setscore(id: number, score: number) {
    console.log(id)

    scorePost.post(
      {
        id, score
      },
      json => {
        scorePost.showToast('post score')
      })

  }


  const rejectPost = usePost("/job/company/reject")

  async function reject(id: number) {
    console.log(id)
    rejectPost.post(
      {
        id
      },
      json => {
        rejectPost.showToast('reject successfully')
      })
    list.refresh()

  }



  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.company.home}></IonBackButton>
          </IonButtons>
          <IonTitle>employee</IonTitle>
          <IonButton slot="end" routerLink={routes.message("job",job_id)}>
            <IonIcon  icon={personCircle}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent className='ion-padding'>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">employee</IonTitle>
          </IonToolbar>
        </IonHeader>
        {list.render((json) => json.list.map(list =>

          <div className='jobEmployeeContainer'>
            <div><div>ID:{list.employee_id}</div><div>名稱:<br></br>{list.nickname}</div></div>
              <div><div><IonLabel>獎懲金額:</IonLabel>
              <IonInput value={specialAward} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}} type="number" name='Project Name' placeholder="Award" onIonChange={e => setSpecialAward(e.detail.value!)}></IonInput></div>

              <IonButton onClick={(e) => awards(list.id, specialAward)}>submit</IonButton></div>
              <div><div><IonLabel>評分:</IonLabel>
              <IonSelect value={score} style={{border: "1px solid rgb(101, 101, 101)",borderRadius:"10px"}}  name='Project Name'  onIonChange={e => setScore(e.detail.value!)}>
              <IonSelectOption value= {1} > 1 </IonSelectOption>
              <IonSelectOption value= {2} > 2 </IonSelectOption>
              <IonSelectOption value= {3} > 3 </IonSelectOption>
              <IonSelectOption value= {4} > 4 </IonSelectOption>
              <IonSelectOption value= {5} > 5 </IonSelectOption>
              </IonSelect></div>
              <IonButton onClick={(e) => setscore(list.id, score)}>submit</IonButton></div>

            <div><IonButton onClick={(e) => reject(list.id)} >刪除</IonButton></div>
          </div>

        ))}

        <div className='jobEmployeeContainer'>總人數:{list.render((json) => json.list.length)}</div>
      </IonContent>
    </IonPage>
  );
};


export default JobEmployeeList;