import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import DataTable from 'react-data-table-component';


// import SelectInputComp from './components/SelectInputComp';
import NumberInputComp from './components/NumberInputComp';
import TextInputComp from './components/TextInputComp';
// import DateInputComp from './components/DateInputComp';

const columns = [
  {
    name: 'admin_id',
    selector: (row: { admin_id: any; }) => row.admin_id,
  },
  {
      name: 'district_id',
      selector: (row: { district_id: any; }) => row.district_id,
  },
  {
      name: 'company_id',
      selector: (row: { company_id: any; }) => row.company_id,
  },
  {
    name: 'name',
    selector: (row: { name: any; }) => row.name,
  },
  {
      name: 'salary',
      selector: (row: { salary: any; }) => row.salary,
  },
  {
    name: 'latlng',
    selector: (row: { latlng: any; }) => row.latlng,
  },
  {
    name: 'place',
    selector: (row: { place: any; }) => row.place,
  },
  {
    name: 'require_worker_num',
    selector: (row: { require_worker_num:any; }) => row.require_worker_num,
  },
  {
    name: 'other',
    selector: (row: { other: any; }) => row.other,
  },
  {
    name: 'start_date',
    selector: (row: { start_date: any; }) => row.start_date,
  },
  {
    name: 'end_date',
    selector: (row: { end_date: any; }) => row.end_date,
  },
  {
    name: 'daily_start_time',
    selector: (row: { daily_start_time: any; }) => row.daily_start_time,
  },
  {
    name: 'daily_end_time',
    selector: (row: { daily_end_time: any; }) => row.daily_end_time,
  },
  
];


//hard code data
const data = [
  {
    admin_id:1,
    district_id:1, 
    company_id:1,
    name:"動漫節",
    salary:2000,
    latlng:"(22.374477,114.1082506)",
    place:"梨木樹",
    require_worker_num:5,
    other:"testing",
    start_date:"2022-03-10 10:00:00+08",
    end_date:"2022-03-12 20:00:00+08",
    daily_start_time:"10:00:00",
    daily_end_time :"20:00:00",
  },
  {
    admin_id:2,
    district_id:2, 
    company_id:2,
    name:"中秋節",
    salary:4000,
    latlng:"(22.374477,114.1082506)",
    place:"香港仔",
    require_worker_num:6,
    other:"testing",
    start_date:"2022-03-10 10:00:00+08",
    end_date:"2022-03-12 20:00:00+08",
    daily_start_time:"10:00:00",
    daily_end_time :"20:00:00",
  },
]

const tableData = data.map( v => {
  return {
    admin_id: v.admin_id,
    district_id: v.district_id,
    company_id: v.company_id,
    name: <TextInputComp defaultValue={v.name} userId={v.admin_id} attribute={"name"}/>,
    salary: <NumberInputComp defaultValue={v.salary} userId={v.admin_id} attribute={"salary"}/>,
    latlng: <NumberInputComp defaultValue={v.latlng} userId={v.admin_id} attribute={"latlng"}/>,
    place: <TextInputComp defaultValue={v.place} userId={v.admin_id} attribute={"place"}/>,
    require_worker_num: <NumberInputComp defaultValue={v.require_worker_num} userId={v.admin_id} attribute={"require_worker_num"}/>,
    other: <TextInputComp defaultValue={v.other} userId={v.admin_id} attribute={"other"}/>,
    start_date: <TextInputComp defaultValue={v.start_date} userId={v.admin_id} attribute={"start_date"}/>,
    end_date: <TextInputComp defaultValue={v.end_date} userId={v.admin_id} attribute={"end_date"}/>,
    daily_start_time: <TextInputComp defaultValue={v.daily_start_time} userId={v.admin_id} attribute={"daily_start_time"}/>,
    daily_end_time: <TextInputComp defaultValue={v.daily_end_time} userId={v.admin_id} attribute={"daily_end_time"}/>,
  }
})




const projectApproval: React.FC = () => {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>工作項目</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
          <DataTable
            columns={columns}
            data={tableData}
          />
        </IonContent>
      </IonPage>
    );
  };
  
  export default projectApproval;