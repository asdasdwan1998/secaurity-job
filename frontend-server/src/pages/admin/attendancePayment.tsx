import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import DataTable from 'react-data-table-component';


const columns = [
  {
      name: '員工名單',
      selector: (row: { title: any; }) => row.title,
  },
  {
      name: '參與項目',
      selector: (row: { project: any; }) => row.project,
  },
  {
      name: '出勤率',
      selector: (row: { participation: any; }) => row.participation,
  },
  {
    name: '員工電話',
    selector: (row: { mobile:number; }) => row.mobile,
  },
  {
    name: '開始日期',
    selector: (row: { startDate: any; }) => row.startDate,
  },
  {
    name: '結束日期',
    selector: (row: { endDate: any; }) => row.endDate,
  },
  {
    name: '出勤費用',
    selector: (row: { salary: any; }) => row.salary,
  },
  {
    name: '出勤狀態',
    selector: (row: { status: any; }) => row.status,
  },
  {
    name: '付款方式',
    selector: (row: { paymentMethod: any; }) => row.paymentMethod,
  },
  {
    name: '付款日期',
    selector: (row: { paymentDate: any; }) => row.paymentDate,
  },
  
];


//hard code data
const data = [
  {
      id: 1,
      title: '員工1',
      project:'傷心大廈',
      participation:'100%',
      mobile:12345678,
      startDate:'12/12/12',
      endDate:'12/12/12',
      salary: '15000',
      status:'paid',
      paymentMethod:'payme',
      paymentDate:'12/12/12',
  },
  {
      id: 2,
      title: '員工2',
      project:'傷心大廈',
      participation:'100%',
      mobile:87654321,
      startDate:'12/12/12',
      endDate:'12/12/12',
      salary: '15000',
      status:'not settle yet',
      paymentMethod:'cash',
      paymentDate:'12/12/12',
  },
  {
      id: 3,
      title: '員工3',
      project:'傷心大廈',
      participation:'100%',
      mobile:87654321,
      startDate:'12/12/12',
      endDate:'12/12/12',
      salary: '15000',
      status:'pending',
      paymentMethod:'cash',
      paymentDate:'12/12/12',
  },
  {
    id: 4,
    title: '員工3',
    project:'傷心大廈',
    participation:'100%',
    mobile:87654321,
    startDate:'12/12/12',
    endDate:'12/12/12',
    salary: '15000',
    status:'pending',
    paymentMethod:'cash',
    paymentDate:'12/12/12',
},
{
  id: 5,
  title: '員工3',
  project:'傷心大廈',
  participation:'100%',
  mobile:87654321,
  startDate:'12/12/12',
  endDate:'12/12/12',
  salary: '15000',
  status:'pending',
  paymentMethod:'cash',
  paymentDate:'12/12/12',
},
  
]

const attendancePayment: React.FC = () => {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>出勤/付款管理</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
          <DataTable
            columns={columns}
            data={data}
          />
        </IonContent>
      </IonPage>
    );
  };
  
  export default attendancePayment;