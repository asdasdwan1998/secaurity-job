import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonButton } from '@ionic/react';
import { routes } from '../../routes';


const WebsiteHomePage: React.FC = () => {
  return (
    <IonPage>

      <IonHeader>
        <IonToolbar>
          <IonTitle>Admin Menu Section</IonTitle>
        </IonToolbar>
      </IonHeader>


      <IonContent fullscreen>
        <IonHeader>
          <IonToolbar>
            <IonButton className='button' expand='block' routerLink={routes.admin.companyList}>管理物管公司名單</IonButton>  
            <IonButton className='button' expand='block' routerLink={routes.admin.companyApproval}>公司審批管理</IonButton>
            <IonButton className='button' expand='block' routerLink={routes.admin.projectDetails}>工作項目審批管理</IonButton>
            <IonButton className='button' expand='block' routerLink={routes.admin.attendancePayment}>出勤/付款管理</IonButton>
            <IonButton className='button' expand='block' routerLink={routes.admin.rewardSystem}>獎賞系統管理</IonButton>  
            <IonButton className='button' expand='block' routerLink={routes.admin.basicData}>基本資料管理</IonButton>
            <IonButton className='button' expand='block' routerLink={routes.admin.reportStatement}>報告/報表管理</IonButton>
          </IonToolbar>
        </IonHeader>
      </IonContent>
    </IonPage>
  );
};

export default WebsiteHomePage;
