import { env } from "../../../env"
import { store } from "../../../redux/store"

let apiOrigin = env.API_ORIGIN

async function updateCompanyList(
    val:string | number, 
    userId:number, 
    attribute:string, 
    ){
        let token = store.getState().auth.user?.token

        let obj = { 
            val:val,
            companyId:userId,
            attribute:attribute,
        }

        let res = await fetch( apiOrigin + "/admin/updateCompanyInfo",{
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token
            },
            body: JSON.stringify(obj)
        })

        let result = await res.json();
        console.log(result);
}

async function updateEmployeeApproval(
    val:string | number, 
    userId:number, 
    attribute:string, 
    ){
        let token = store.getState().auth.user?.token

        let obj = { 
            val:val,
            userId:userId,
            attribute:attribute,
        }

        let res = await fetch( apiOrigin + "/admin/updateUserAccount",{
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token
            },
            body: JSON.stringify(obj)
        })

        let result = await res.json();
        console.log(result);
}

export { updateCompanyList, updateEmployeeApproval }