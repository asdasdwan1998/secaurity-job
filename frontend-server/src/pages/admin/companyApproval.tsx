import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import DataTable from 'react-data-table-component';

const columns = [
  {
      name: '申請公司名稱',
      selector: (row: { title: any; }) => row.title,
  },
  {
    name: '公司負責人',
    selector: (row: { leader: any; }) => row.leader,
  },
  {
      name: '公司電話',
      selector: (row: { companyNumber: any; }) => row.companyNumber,
  },
  {
      name: '公司地址',
      selector: (row: { address: any; }) => row.address,
  },
  {
    name: '電郵地址',
    selector: (row: { email: any; }) => row.email,
  },
  {
    name: '銀行賬戶',
    selector: (row: { bankAccount:number; }) => row.bankAccount,
  },
  {
    name: '申請日期',
    selector: (row: { applyTime:any; }) => row.applyTime,
  },
  {
    name: '申请狀態',
    selector: (row: { status:any; }) => row.status,
  },
  
];


//hard code data
const data = [
  {
      id: 1,
      title: '公司1',
      leader:'陳小明',
      companyNumber:'12345678',
      address:'TW 5/F',
      year: '1988',
      email:'company1@gmail.com',
      bankAccount:2222444466668888,
      applyTime:'12/12/12',
      status:'成功',
     
  },
  {
      id: 2,
      title: '公司2',
      leader:'周星星',
      companyNumber:'23456789',
      address:'TW 5/F',
      year: '1984',
      email:'company2@gmail.com',
      bankAccount:8888666611112222,
      applyTime:'12/12/12',
      status:'成功',
      
  },

]

const companyApproval: React.FC = () => {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>公司審批</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
          <DataTable
          columns={columns}
          data={data}
          />
        </IonContent>
      </IonPage>
    );
  };
  
  export default companyApproval;