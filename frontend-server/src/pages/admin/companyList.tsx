import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import DataTable from 'react-data-table-component';
import SelectInputComp from './components/SelectInputComp';
import NumberInputComp from './components/NumberInputComp';
import TextInputComp from './components/TextInputComp';
// import DateInputComp from './components/DateInputComp';
// import { analyticsSharp } from 'ionicons/icons';
import { useGet } from '../../hooks/use-get';
// import { useEffect, useState } from 'react';


const CompanyList: React.FC = () => {

  const profile = useGet('/admin/companyList',{
    error:"",
		id: "",
		name: "",
		telephone: "",
		address: "",
		email: "",
		bank_account: "",
		principal: "",
		enable: "",
  },)

  const columns:any = [
    {
      name: 'id',
      selector: (row: { id: any; }) => row.id,
    },
    {
      name: 'name',
        selector: (row: { name: any; }) => row.name,
    },
    {
        name: 'telephone',
        selector: (row: { telephone: any; }) => row.telephone,
    },
    {
        name: 'address',
        selector: (row: { address: any; }) => row.address,
    },
    {
      name: 'bank_account',
      selector: (row: { bank_account: any; }) => row.bank_account,
    },
    {
      name: 'email',
      selector: (row: { email: any; }) => row.email,
    },
    {
      name: 'principal',
      selector: (row: { principal:any; }) => row.principal,
    },
    {
      name: 'enable',
      selector: (row: { enable:any; }) => row.enable,
    },
  ];
  
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>公司名單</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
         <DataTable
          columns={columns}
          data={ 
            Array.isArray(profile.state) ? 
            profile.state.map( (v:any) => {
              return {
                id: v.id,
                name: <TextInputComp defaultValue={v.name} userId={v.id} attribute={"name"} tableName={"companyList"}/>,
                telephone: <NumberInputComp defaultValue={v.telephone} userId={v.id} attribute={"telephone"}  tableName={"companyList"}/>,
                address: <TextInputComp defaultValue={v.address} userId={v.id} attribute={"address"} tableName={"companyList"} />,
                email: <TextInputComp defaultValue={v.email} userId={v.id} attribute={"email"} tableName={"companyList"} />,
                bank_account: <NumberInputComp defaultValue={v.bank_account} userId={v.id} attribute={"bank_account"}  tableName={"companyList"}/>,
                principal: <TextInputComp defaultValue={v.principal} userId={v.id} attribute={"principal"} tableName={"companyList"}/>,
                enable: <SelectInputComp defaultValue={v.enable} data={['YES', 'NO']} userId={v.id} attribute={"enable"}  tableName={"companyList"}/>,
              }
            })
            : []
          }
          />
  
        </IonContent>
      </IonPage>
    );
  };
  
  export default CompanyList;