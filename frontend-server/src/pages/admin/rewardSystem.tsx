import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import DataTable from 'react-data-table-component';
// import { useState } from 'react';


import SelectInputComp from './components/SelectInputComp';
import NumberInputComp from './components/NumberInputComp';
import TextInputComp from './components/TextInputComp';
import DateInputComp from './components/DateInputComp';

const columns = [
  {
      name: '所有員工名單',
      selector: (row: { title: any; }) => row.title,
  },
  {
    name: '獎賞項目 1',
    selector: (row: { rewardItem: any; }) => row.rewardItem,
  },
  {
    name: '獎賞項目 2',
    selector: (row: { rewardItem: any; }) => row.rewardItem,
  },
  {
    name: '獎賞項目 3',
    selector: (row: { rewardItem: any; }) => row.rewardItem,
  },
  // {
  //     name: '好友介紹獎金',
  //     selector: (row: { friendIntro: any; }) => row.friendIntro,
  // },
  // {
  //     name: '每月之星獎賞',
  //     selector: (row: { monthlyStar: any; }) => row.monthlyStar,
  // },
  // {
  //   name: '優異排位5星獎賞',
  //   selector: (row: { excellentFive: any; }) => row.excellentFive,
  // },
  // {
    //   name: '迎新禮物(1hr)',
    //   selector: (row: { newPresents: any; }) => row.newPresents,
    // },
  {
    name: '長期服務獎金(HKD)',
    selector: (row: { longServicePayment:any; }) => row.longServicePayment,
  },
  {
    name: '總費用',
    selector: (row: { totalExpense: any; }) => row.totalExpense,
  },
  {
    name: '付款日期',
    selector: (row: { paymentDate: any; }) => row.paymentDate,
  },
  {
    name: '付款方式',
    selector: (row: { paymentMethod: any; }) => row.paymentMethod,
  },
  {
    name: 'paymentStatus',
    selector: (row: { paymentStatus: any; }) => row.paymentStatus,
  },
];


//hard code data
const data = [
  {
      id: 1,
      title: '員工1',
      rewardItem:'',
      // friendIntro:'YES',
      // monthlyStar:'YES',
      // excellentFive:'YES',
      // newPresents:'YES',
      longServicePayment:'',
      totalExpense:3000,
      paymentDate:'2022-12-15T13:47:20.789',
      paymentMethod:'Payme',
      paymentStatus:'',

  },
  {
      id: 2,
      title: '員工2',
      rewardItem:'',
      // friendIntro:'YES',
      // monthlyStar:'YES',
      // excellentFive:'YES',
      longServicePayment:'',
      newPresents:'YES',
      totalExpense:3000,
      paymentDate:'2032-12-18T13:47:20.789',
      paymentMethod:'Bank Transfer',
      paymentStatus:'',

  },
  
]

const tableData = data.map( v => {
  return {
    id: v.id,
    title: <TextInputComp defaultValue={v.title} userId={v.id} attribute={"title"}/>,
    rewardItem: <SelectInputComp defaultValue={v.rewardItem} data={['好友介紹獎金', '每月之星獎賞', '優異排位5星獎賞','迎新禮物','Null']} userId={v.id} attribute={"rewardItem"}/>,
    longServicePayment: <NumberInputComp defaultValue={v.longServicePayment} userId={v.id} attribute={"longServicePayment"}/>,
    totalExpense: <NumberInputComp defaultValue={v.totalExpense} userId={v.id} attribute={"totalExpense"}/>,
    paymentDate: <DateInputComp defaultValue={v.paymentDate} userId={v.id} attribute={"paymentDate"}/>,
    paymentMethod: <SelectInputComp defaultValue={v.paymentMethod} data={['Payme', 'Bank Transfer', '現金']} userId={v.id} attribute={"paymentMethod"}/>,
    paymentStatus: <SelectInputComp defaultValue={v.paymentStatus} data={['Haven\'t yet ','Paid']} userId={v.id} attribute={"paymentStatus"}/>,

  }
})


const rewardSystem: React.FC = () => {
    return (
      <IonPage>

        <IonHeader>
          <IonToolbar>
            <IonTitle>獎賞系統管理</IonTitle>
          </IonToolbar>
        </IonHeader>


        <IonContent fullscreen>
          <DataTable
          columns={columns}
          data={tableData}
          />
        </IonContent>
      </IonPage>
    );
  };
  
  export default rewardSystem;