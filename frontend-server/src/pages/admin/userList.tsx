import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import DataTable from 'react-data-table-component';

import SelectInputComp from './components/SelectInputComp';
import NumberInputComp from './components/NumberInputComp';
import TextInputComp from './components/TextInputComp';
// import DateInputComp from './components/DateInputComp';
// import { analyticsSharp } from 'ionicons/icons';
import { useGet } from '../../hooks/use-get';
// import { useEffect, useState } from 'react';



const UserList: React.FC = () => {

  const userList = useGet('/admin/userList',{
    error:"",
		id: "",
		nickname: "",
    email:"",
    mobile: "",
    icon: "",
    enable: "",
    referrer_id: "",
    invite_code: "",
  },)

  const columns:any = [
    {
      name: 'id',
      selector: (row: { id: any; }) => row.id,
    },
    {
        name: 'nickname',
        selector: (row: { nickname: any; }) => row.nickname,
    },
    {
      name: 'email',
      selector: (row: { email: any; }) => row.email,
    },
    {
      name: 'mobile',
      selector: (row: { mobile:any; }) => row.mobile,
    },
    {
      name: 'icon',
      selector: (row: { icon:any; }) => row.icon,
    },
    {
      name: 'enable',
      selector: (row: { enable:any; }) => row.enable,
    },
    {
      name: 'referrer_id',
      selector: (row: { referrer_id:any; }) => row.referrer_id,
    },
    {
      name: 'invite_code',
      selector: (row: { invite_code:any; }) => row.invite_code,
    },
  
  ];
  

    return (
      <IonPage>

        <IonHeader>
          <IonToolbar>
            <IonTitle>員工名單</IonTitle>
          </IonToolbar>
        </IonHeader>


        <IonContent fullscreen>
          <DataTable
            columns={columns}
            data={ 
              Array.isArray(userList.state) ? 
              userList.state.map( (v:any) => {
                return {
                  id: v.id,
                  nickname: <TextInputComp defaultValue={v.nickname} userId={v.id} attribute={"nickname"}/>,
                  email: <TextInputComp defaultValue={v.email} userId={v.id} attribute={"email"}/>,
                  mobile: <NumberInputComp defaultValue={v.mobile} userId={v.id} attribute={"mobile"}/>,
                  icon: <TextInputComp defaultValue={v.icon} userId={v.id} attribute={"icon"}/>,  //change type ?
                  enable: <SelectInputComp defaultValue={v.enable} data={['YES', 'NO']} userId={v.id} attribute={"enable"}/>,
                  referrer_id: <NumberInputComp defaultValue={v.referrer_id} userId={v.id} attribute={"referrer_id"}/>,
                  invite_code: <NumberInputComp defaultValue={v.invite_code} userId={v.id} attribute={"invite_code"}/>,
                }
              })
              : []}
          />
        </IonContent>
      </IonPage>
    );
  };
  
  export default UserList;