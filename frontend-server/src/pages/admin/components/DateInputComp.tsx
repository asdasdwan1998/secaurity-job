import {  IonDatetime, IonItem, IonLabel, IonPopover, IonText  } from "@ionic/react";
import { useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import { uploadHandler } from "./updateUtilis";

function DateInputComp({ defaultValue, userId, attribute, tableName }:any){
    const [ currentValue, setCurrentValue ] = useState<string>(defaultValue)
    const uid = uuidv4();
  
    async function changeHandler(val:string){
      setCurrentValue(val);
      console.log(val);

      let isSuccess = await uploadHandler(val, userId, attribute, tableName);
      console.log(isSuccess);
      //fetch
    }
  
    return (
      <div>
       <IonItem button={true} id={uid}>
        <IonLabel>Date</IonLabel>
        <IonText slot="end">{currentValue}</IonText>
        <IonPopover trigger={uid} showBackdrop={false}>
          <IonDatetime
            presentation="date"
            onIonChange={e => changeHandler(e.detail.value!)}
          />
        </IonPopover>
      </IonItem>    

      </div>
    )
}

export default DateInputComp