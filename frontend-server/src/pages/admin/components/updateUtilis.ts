import { updateCompanyList, updateEmployeeApproval } from "../api/dataAPI";

async function uploadHandler(val:string | number, userId:number, attribute:string, tableName:string){

    if(!val || !userId || !attribute || !tableName){
        return false;
    }

    console.log("recived");

    if(tableName === "companyList"){ // companyList update
        updateCompanyList(val, userId, attribute);
    }
    else if(tableName === "employeeApproval"){         // employeeApproval update
        updateEmployeeApproval(val, userId, attribute);
    }

    return true

}

export { uploadHandler }