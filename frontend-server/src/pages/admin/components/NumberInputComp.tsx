import { IonInput  } from "@ionic/react";
import { useState } from "react";
import { uploadHandler } from "./updateUtilis";

function NumberInputComp({ defaultValue, userId, attribute, tableName }:any){
    const [ currentValue, setCurrentValue ] = useState<string>(defaultValue)
  
    async function changeHandler(val:string){
      setCurrentValue(val);
      console.log(val);

      let isSuccess = await uploadHandler(val, userId, attribute, tableName);
      console.log(isSuccess);

      //fetch
    }
  
    return (
      <div>
        <IonInput type="number" value={currentValue} placeholder="Enter Input" onIonChange={e => changeHandler(e.detail.value!)} />
      </div>
    )
}

export default NumberInputComp