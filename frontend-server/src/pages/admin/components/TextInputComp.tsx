import { IonInput  } from "@ionic/react";
import { useState } from "react";
import { uploadHandler } from "./updateUtilis";

function TextInputComp({ defaultValue, userId , attribute, tableName }:any){
    const [ currentValue, setCurrentValue ] = useState<string>(defaultValue)
  
    async function changeHandler(val:string){
      setCurrentValue(val);

      let isSuccess = await uploadHandler(val, userId, attribute, tableName);
      console.log(isSuccess);

    }
  
    return (
      <div>
        <IonInput type="text" value={currentValue} placeholder="Enter Input" onIonChange={e => changeHandler(e.detail.value!)} />
      </div>
    )
}

export default TextInputComp