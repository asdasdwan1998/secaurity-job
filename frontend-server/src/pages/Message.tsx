import {
  IonPage,
  IonContent,
  IonIcon,
  IonButton,
  IonAvatar,
  IonInput,
} from "@ionic/react";
import { chevronBackOutline, sendSharp } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useGet } from "../hooks/use-get";
import { usePost } from "../hooks/use-post";
import { useRole } from "../hooks/useRole";
import { routes } from "../routes";
import "./Message.css";
import { useSocket } from "../hooks/use-socket";

const MessagePage: React.FC = () => {
  const { type, id } = useParams<{ type: string; id: string }>();
  const [last, setLast] = useState<HTMLElement | null>(null);
  useEffect(() => {
    if (last) {
      last.scrollIntoView({ behavior: "auto" });
    }
  }, [last]);
  let employee_id = useRole()?.id;
  const [item] = useState(20);
  const messageList = useGet(`/message/${type}/${id}?item=${item}`, {
    error: "",
    data: [] as {
      id: number;
      content: string;
      user_id: number;
      nickname: string | null;
      job_id: number | null;
      send_time: Date;
    }[],
  });
  const messageInfo = useGet(`/message/info/${type}/${id}`, {
    error: "",
    room: {
      icon: "",
      roomName: "",
    },
    online: {
      status: "",
    },
  });
  const [content, setContent] = useState("");
  const sendMessage = usePost("/message/" + type + "/" + id);

  async function submitContent() {
    if (!content) {
      return;
    }

    sendMessage.post({ content }, (json) => {
      setContent("");
    });
  }

  let company_id = useRole()?.company_id;
  let route = company_id
    ? routes.company.messageList
    : routes.employee.messageList;
  useSocket((socket) => {
    if (type === "user") {
      socket.emit("join", `${type}-${employee_id}`);
      socket.emit("join", `${type}-${id}`);
    } else {
      socket.emit("join", `${type}-${id}`);
    }
    
    socket.on(`newMessage`, (message) => {
      messageList.setState((json) => {
        return { ...json, data: [...(json.data || []), message.data ] };
      });
    });
  });

  // const [data, setData] = useState<string[]>([]);
  // const [isInfiniteDisabled, setInfiniteDisabled] = useState(false);
  // const pushData = () => {
  //   const max = data.length + 20;
  //   const min = max - 20;
  //   const newData = [];
  //   for (let i = min; i < max; i++) {
  //     newData.push('Item' + i);
  //   }

  //   setData([
  //     ...data,
  //     ...newData
  //   ]);
  // }

  // const loadData = (ev: any) => {
  //   setTimeout(() => {
  //     pushData();
  //     console.log('Loaded data');
  //     ev.target.complete();
  //     if (data.length === 1000) {
  //       setInfiniteDisabled(true);
  //     }
  //   }, 500);
  // }

  return (
    <IonPage className="message">
      {/* <IonHeader class='ion-no-border'>
      </IonHeader> */}
      <IonContent>
        <div className="header">
          <div className="info">
            <IonButton fill="clear" routerLink={route}>
              <IonIcon icon={chevronBackOutline}></IonIcon>
            </IonButton>
            {messageInfo.render((json) => {
              return (
                <>
                  <IonAvatar>
                    <img
                      src={
                        json.room?.icon || "./assets/image/companylogo2nd.png"
                      }
                      onError={(e) => {
                        e.currentTarget.src = "./assets/image/default.png";
                      }}
                      alt=""
                    />
                  </IonAvatar>
                  <span className="details">
                    <h1>{json.room?.roomName}</h1>
                    <p>{json.online?.status}</p>
                  </span>
                </>
              );
            })}
          </div>
        </div>
        <div className="conversation ion-padding">
          <div className="chat-section" id="chat">
            <div className="chat">
              {messageList.render((json) =>
                json.data
                  .map((message) => {
                    let time = new Date(message.send_time);
                    let now = new Date();
                    let hour = time.getHours();
                    let data = time.getDate();
                    let month = time.getMonth() + 1;
                    let minutes = time.getMinutes();
                    let year = time.getFullYear();
                    let enTime;
                    if (hour <= 12) {
                      enTime = "am";
                    } else if (hour > 12) {
                      enTime = "pm";
                    }
                    let send_time;
                    if (
                      time.setHours(0, 0, 0, 0) === now.setHours(0, 0, 0, 0)
                    ) {
                      send_time = hour + ":" + minutes + " " + enTime;
                    } else {
                      send_time =
                        data +
                        "/" +
                        month +
                        "/" +
                        year +
                        " " +
                        hour +
                        ":" +
                        minutes +
                        " " +
                        enTime;
                    }
                    return { message, send_time };
                  })
                  .map(({ message, send_time }, i, msgList) => {
                    return (
                      <div
                        key={message.id}
                        className={
                          "messages " +
                          (message.user_id === employee_id ? "mine" : "yours")
                        }
                      >
                        <div
                          className={
                            "message " +
                            (msgList[i + 1]?.message.user_id !== message.user_id
                              ? "last"
                              : "")
                          }
                        >
                          {message.user_id !== employee_id ? (
                            message.job_id ? (
                              <div className="username">{message.nickname}</div>
                            ) : null
                          ) : null}
                          <div>{message.content}</div>
                        </div>
                        {msgList[i + 1]?.message.user_id !== message.user_id ? (
                          <span>{send_time}</span>
                        ) : msgList[i + 1]?.send_time !== send_time ? (
                          <span>{send_time}</span>
                        ) : null}
                      </div>
                    );
                  })
              )}
              <p
                key="last"
                ref={(e) => {
                  if (!last && e) {
                    setLast(e);
                  }
                }}
              ></p>
              {/* <IonInfiniteScroll
          onIonInfinite={loadData}
          threshold="100px"
          position="top"
          disabled={isInfiniteDisabled}
        >
          <IonInfiniteScrollContent
            loadingSpinner="bubbles"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll> */}

              {/* <div className="yours messages">
                <div className="message other">Hey!</div>
                <div className="message other">Hey!</div>
                <div className="last-message-other">
                  <IonAvatar>
                    <img src="assets/image/default.png" alt="" />
                  </IonAvatar>
                  <div className="message last">Hello, how's going</div>
                </div>
                <span>1:4 PM</span>
              </div>
              <div className="mine messages">
                <div className="message">Great thanks</div>
                <div className="message last">How about?</div>
                <span>
                  <IonIcon icon={checkmarkDoneOutline} />
                  1:44pm
                </span>
              </div> */}
            </div>
          </div>
          <div className="message-input">
            <IonInput
              placeholder="Type your message..."
              onIonChange={(e) => setContent(e.detail.value!)}
            ></IonInput>
            <div className="send-button" onClick={() => submitContent()}>
              <IonIcon icon={sendSharp}></IonIcon>
            </div>
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default MessagePage;
