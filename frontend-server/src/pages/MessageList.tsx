import {
  IonAvatar,
  IonBadge,
  IonButton,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { personCircle, searchSharp } from "ionicons/icons";
import "./MessageList.css";
import { useGet } from "../hooks/use-get";
import { useRole } from "../hooks/useRole";
import { routes } from "../routes";


const MessageItem = () => {
  let employee_id = useRole()?.id
  const messageList = useGet("/message/list", {
    error: "",
    data: [] as {
      content: string;
      id: number;
      job_id: number | null;
      job_name: string | null;
      receiver_icon: string | null;
      receiver_id: number | null;
      receiver_name: string | null;
      send_time: Date;
      sender_icon: string | null;
      sender_id: number | null;
      sender_name: string | null;
    }[],
  });

  return (<>{messageList.render((json)=> json.data.map(message  => {
    let name
    let icon
    let roomID
    if(message.job_id){
      name = message.job_name
      roomID = "/message/job/" + message.job_id
    } else if ( message.sender_id === employee_id) {
      icon = message.receiver_icon
      name = message.receiver_name
      roomID = "/message/user/" + message.receiver_id
    } else {
      icon = message.sender_icon
      name = message.sender_name
      roomID = "/message/user/" + message.sender_id
    }
    let time = new Date(message.send_time)
    let now = new Date()
    let hour = time.getHours()
    let data = time.getDate()
    let month = time.getMonth() +1 
    let minutes = time.getMinutes()
    let send_time 
  if(time.setHours(0,0,0,0) === now.setHours(0,0,0,0)){
    send_time = hour+":"+ minutes
  } else {
    send_time = data +"/"+ month
  }

return <IonItem key={message.id} lines="none" routerLink={roomID} detail={false}>
      <IonAvatar slot="start">
        <img alt="" src={icon || "./assets/image/companylogo2nd.png"} onError={(e) => {
                e.currentTarget.src = "assets/image/default.png";
              }}/>
      </IonAvatar>
      <IonLabel className="ion-text-nowrap">
        <h2>{name}</h2>
        <p>{message.content}</p>
      </IonLabel>
      <div className="info-div">
        <IonBadge className="ion-margin-top"></IonBadge>
        <p>{send_time}</p>
      </div>
    </IonItem>
  }))}</>
    
    
  );
};

const MessageListPage: React.FC = () => {
  return (
    <IonPage className="messageList">
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonTitle>Message</IonTitle>
          <IonButton slot="end" routerLink={routes.message("user",4)}>
            <IonIcon  icon={personCircle}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        {/* <div className="tabs ion-padding">
          <IonSegment value="chats" onIonChange={(e) => console.log(e)}>
            <IonSegmentButton value='chats'>
              <IonLabel>Chats</IonLabel>
              </IonSegmentButton>
            <IonSegmentButton value='status'>
              <IonLabel>Status</IonLabel>
              </IonSegmentButton>
            <IonSegmentButton value='calls'>
              <IonLabel>Calls</IonLabel>
              </IonSegmentButton>
              </IonSegment>
          </div> */}
        <div className="chat-list">
          {/* <div className='recent-chats ion-padding'>
              <div className="heading">
                <h1>Recent Chats</h1>
                <IonIcon icon={searchSharp}></IonIcon>
                </div>
                <div className='chats'>
                  <IonItem lines='none' routerLink='/page'>
                      <IonAvatar slot='start'>
                        <img src="assets/image/default.png" alt=''/>
                        </IonAvatar>
                        <IonLabel className='ion-text-nowrap'>
                          <h2>Bella</h2>
                          <p>Typing...</p>
                          </IonLabel>
                          <div className='info-div'>
                            <IonBadge>1</IonBadge>
                            <p>2:13</p>
                            </div>
                    </IonItem>
                    </div>
                </div> */}
          <div className="all-chats ion-padding">
            <div className="heading">
              <h1>All Chats</h1>
              <IonIcon icon={searchSharp}></IonIcon>
            </div>
            <div className="chats">
              {/* <IonItem lines="none" routerLink="/message" detail={false}>
                <IonAvatar slot="start">
                  <img src="assets/image/default.png" alt="" />
                </IonAvatar>
                <IonLabel className="ion-text-nowrap">
                  <h2>Bella</h2>
                  <p>Typing...</p>
                </IonLabel>
                <div className="info-div">
                  <IonBadge className="ion-margin-top">100</IonBadge>
                  <p>2:13</p>
                </div>
              </IonItem> */}
              <MessageItem />
            </div>
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default MessageListPage;
