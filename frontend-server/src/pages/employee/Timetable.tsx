import { IonContent, IonDatetime, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';

const EmployeeTimetable: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>時間表</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div className="grid-item">
            {/* <h5>查看工作時間表</h5> */}
            <IonDatetime value="2012-12-15T13:47:20.789"></IonDatetime>
          </div>
      </IonContent>
    </IonPage>
  );
};

export default EmployeeTimetable;