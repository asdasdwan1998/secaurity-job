import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
  IonIcon,
  IonLabel,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonThumbnail,
  IonItem,
  IonList,
  useIonToast,
  useIonAlert,
} from "@ionic/react";
import { calendar, clipboard, logOut, wallet } from "ionicons/icons";
import { useCallback, useEffect, useState } from "react";
import { routes } from "../../routes";
import "./HomePage.css";
import {useGet}from"../../hooks/use-get"
import { post } from "../../api";
import { logoutThunk } from "../../redux/auth/thunk";
import { useDispatch } from "react-redux";

function formatDuration(duration: number) {
  let ms = duration % 1000;
  let remind = (duration - ms) / 1000;
  let second = remind % 60;
  remind = (remind - second) / 60;
  let minute = remind % 60;
  remind = (remind - minute) / 60;
  let hour = remind;
  if (hour < 1) {
    return `${minute}  分鐘`;
  }
  return `${hour}  小時 ${minute} 分鐘`;
}

const EmployeeHomePage: React.FC = () => {
  const profile=useGet('/user/profile',{error:"",id:1,mobile:"",nickname:"",email:""})
  const [checkInTime, setCheckInTime] = useState("");
  const [passedText, setPassedText] = useState("");
  const updatePassedText = useCallback( async function updatePassedText() {
    const passed = Date.now() - new Date(checkInTime).getTime();
    setPassedText(formatDuration(passed));
  },[setPassedText, checkInTime]
  )
  useEffect(() => {
    if (!checkInTime) {
      return;
    }
    const timer = setInterval(updatePassedText, 1000 * 5);
    updatePassedText();
    return () => {
      clearInterval(timer);
    };
  }, [checkInTime, updatePassedText]);


  
  function checkIn() {

    alert({
      cssClass: 'my-css',
      // header: 'Alert',
      message: '你已成功打卡',
      buttons: [
    
        { text: 'Ok', handler: (d) => console.log('ok pressed') },
      ],
      onDidDismiss: (e) => console.log('did dismiss'),
    })
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log("position:", position);
        // position.coords.latitude
        // position.coords.longitude
        // position.coords.accuracy
        setCheckInTime(new Date().toISOString());
        post("/checkIn",{position:{
          lat:position.coords.latitude,
          longitude:position.coords.longitude,
          accuracy:position.coords.accuracy
        }})
      },
      (error) => {
        console.log("failed to get geo position:", error);
      }
    );
  }
  async function showSuccess(){
    present("logout sucessfully", 5000);
  }
  const [present] = useIonToast();
  const dispatch = useDispatch();
  async function logout(){
    dispatch(
      logoutThunk(showSuccess)
    );
  }
  const [alert] = useIonAlert();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>主頁</IonTitle>
          <IonButton slot="end" onClick={logout}>
            <IonIcon slot="end" icon={logOut}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Home</IonTitle>
          </IonToolbar>
        </IonHeader>

        {/* <IonButton className="clockIn" expand="block">打卡</IonButton> */}

        <IonButton   expand="block" color="warning" hidden={!!checkInTime} onClick={checkIn} >
        打卡
        </IonButton>
        <div hidden={!checkInTime}>
          <IonCard >
                  <IonCardHeader>
                    <IonCardTitle> 打卡時間: {new Date(checkInTime).toLocaleString()} </IonCardTitle>
                    <IonCardTitle> 已經打卡: {passedText}</IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent></IonCardContent>
                </IonCard>
          {/* <div>check in time: {new Date(checkInTime).toLocaleString()}</div> */}
          {/* <div>passed: {passedText}</div> */}
        </div>
        <IonButton 
          expand="block"
          color="danger"
          hidden={!checkInTime}
          onClick={() => setCheckInTime("")}
        >
           打卡下班
        </IonButton>

        {/* User ID card */}
        <IonCard>
          <IonCardContent>
            <IonList>
              <IonItem>
                <IonThumbnail slot="start">
                 { <img src="https://us.123rf.com/450wm/tomwang/tomwang1906/tomwang190600127/124613733-portrait-of-asian-security-guard.jpg?ver=6" alt=""/>}
                </IonThumbnail>
                <IonLabel>
                {profile.render((profile) =>    
                <div>
                  <IonCardTitle> ID:{profile.id} </IonCardTitle>
                  <IonCardTitle> Name: {profile.nickname} </IonCardTitle>
                  <h3>3.4</h3>
                  </div>)}

                </IonLabel>
              </IonItem>
            </IonList>
          </IonCardContent>
        </IonCard>

        {/* show User project and hours done */}
        <section>
          {/* <div className="d-flex ion-justify-content-around" > */}
            {/* <IonRow className='ion-no-padding'>
              <IonCol size='6'> */}
                <IonCard> 
                  {/* className="jobDone" */}
                  <IonCardHeader>
                    <IonCardTitle>  提示: 請記得打卡上班下班 </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent></IonCardContent>
                </IonCard>
              {/* </IonCol> */}
              {/* <IonCol size='6'> */}
              
              {/* </IonCol> */}
            {/* </IonRow> */}
          {/* </div> */}
        </section>

      
 
    
    
  
    


        {/* menu button  */}

        <section>
          <IonButton className="mainButton ion-padding" size="large" routerLink={routes.employee.Timetable}>
            <div>
              <IonIcon icon={calendar}></IonIcon>
              <div>時間表</div>
            </div>
          </IonButton>

          <IonButton
            className="mainButton salaryBtn ion-padding"
            size="large"
            routerLink={routes.employee.Salary}
          >
            <div>
              <IonIcon icon={wallet}></IonIcon>
              {/* &nbsp; */}
              <br />

              <div>出糧記錄</div>
            </div>
          </IonButton>

          <IonButton
            className="mainButton ion-padding"
            size="large"
            routerLink={routes.employee.JobRequest}
          >
            <div>
              <IonIcon icon={clipboard}></IonIcon>
              <div>工作請求</div>
            </div>
          </IonButton>


          <IonButton
            className="mainButton salaryBtn ion-padding"
            size="large"
            routerLink={routes.employee.JobRequest}
          >
            <div>
              <IonIcon icon={clipboard}></IonIcon>
              <div>即將推出</div>
            </div>
          </IonButton>
        </section>
      </IonContent>
    </IonPage>
  );
};

export default EmployeeHomePage;
