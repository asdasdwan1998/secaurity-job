import React from "react";

import {
  IonAvatar,
  IonContent,
  IonHeader,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter,
} from "@ionic/react";
import { useState } from "react";

const Salary: React.FC = () => {
  const [data, setData] = useState<string[]>([]);
  const [isInfiniteDisabled, setInfiniteDisabled] = useState(false);

  const pushData = () => {
    const max = data.length + 20;
    const min = max - 20;
    const newData = [];
    for (let i = min; i < max; i++) {
      newData.push("Item" + i);
    }

    setData([...data, ...newData]);
  };
  const loadData = (ev: any) => {
    setTimeout(() => {
      pushData();
      console.log("Loaded data");
      ev.target.complete();
      if (data.length === 1000) {
        setInfiniteDisabled(true);
      }
    }, 500);
  };

  useIonViewWillEnter(() => {
    pushData();
  });
let jobs = [
  {
    id:1, title: '康城住宅保安 (早更)',date: '01-01-2020', amount:'$1000', status: 'paid'
  },{
    id:2, title: '警衞員薄扶林',date: '26-03-2021', amount:'41000', status: 'paid'
  },{
    id:3, title: '深灣遊艇俱樂部',date: '09-10-2021', amount:'41000', status: 'paid'
  }
]
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle> 出糧記錄</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Salary</IonTitle>
          </IonToolbar>
        </IonHeader>

        {/* <IonButton
          onClick={() => setInfiniteDisabled(!isInfiniteDisabled)}
          expand="block"
        >
          Toggle Infinite Scroll
        </IonButton> */}
        <IonList hidden> 
        <IonItem>
          <IonLabel>Month</IonLabel>
          <IonSelect value="12">
            <IonSelectOption value="01">January</IonSelectOption>
            <IonSelectOption value="02">February</IonSelectOption>
            <IonSelectOption value="03">March</IonSelectOption>
            <IonSelectOption value="04">April</IonSelectOption>
            <IonSelectOption value="05">May</IonSelectOption>
            <IonSelectOption value="06">June</IonSelectOption>
            <IonSelectOption value="07">July</IonSelectOption>
            <IonSelectOption value="08">August</IonSelectOption>
            <IonSelectOption value="09">September</IonSelectOption>
            <IonSelectOption value="10">October</IonSelectOption>
            <IonSelectOption value="11">November</IonSelectOption>
            <IonSelectOption value="12">December</IonSelectOption>
          </IonSelect>
        </IonItem>

        <IonItem>
          <IonLabel>Year</IonLabel>
          <IonSelect value="1996">
            <IonSelectOption value="2020">2020</IonSelectOption>
            <IonSelectOption value="2021">2021</IonSelectOption>
            <IonSelectOption value="2022">2022</IonSelectOption>
            <IonSelectOption value="2023">2023</IonSelectOption>
          </IonSelect>
        </IonItem>
        </IonList>

    


        <IonList className="ion-margin-top">
          {jobs.map((job, index) => {
            return (
              <IonItem key={index}>
                <IonAvatar slot="start">
                  <img alt=""
                    src={`https://www.gravatar.com/avatar/${
                      index + 10
                    }?d=monsterid&f=y` }
                  />
                </IonAvatar>
                <IonLabel>
                  <h2>{job.title}</h2>
                  <p>Amount: ${job.amount}</p>
                  <p>status: {job.status}</p>
                  <p>date: {job.date}</p>

                </IonLabel>
              </IonItem>
            );
          })}
        </IonList>

        <IonInfiniteScroll
          onIonInfinite={loadData}
          threshold="100px"
          disabled={isInfiniteDisabled}
        >
          <IonInfiniteScrollContent
            loadingSpinner="bubbles"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonContent>
    </IonPage>
  );
};

export default Salary;
