import {
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useState } from "react";
import { formatDateTime } from "../../format";
import { useGet } from "../../hooks/use-get";
import { usePost } from "../../hooks/use-post";

import "./JobRequest.css";

const JobRequest: React.FC = () => {
  const acceptPost = usePost("/job/role/confirm");
  const rejectPost = usePost("/job/company/reject");
  const createJob = usePost("/job/role/create");

  async function accept(job_id: number) {
    console.log(job_id);
    createJob.post({ job_id });
    acceptPost.post({ job_id }, (json) => {
      acceptPost.showToast("accept successfully");
    });
    list.refresh();
  }

  async function reject(job_id: number) {
    console.log(job_id);
    createJob.post({ job_id });
    rejectPost.post({ job_id }, (json) => {
      rejectPost.showToast("reject successfully");
    });
    list.refresh();
  }

  const completeState = useState<"all" | "completed" | "not-complete">(
    "not-complete"
  );

  const list = useGet(
    completeState[0] === "all"
      ? "/job/list/employee"
      : completeState[0] === "completed"
      ? "/job/list/employee"
      : "/job/list/employee",
    {
      error: "",
      list: [] as {
        id: number;
        name: string;
        salary: string;
        place: string;
        require_worker_num: number;
        start_date: string;
        end_date: string;
        daily_start_time: string;
        daily_end_time: string;
      }[],
    }
  );

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle> 工作請求</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Job Request</IonTitle>
          </IonToolbar>
        </IonHeader>

        {list.render((json) =>
          json.list.map((job) => (
            <div className="project-list " key={job.id}>
              <IonCard className="jobCard">
                <IonCardHeader>
                  <IonCardSubtitle>項目名稱:{job.name}</IonCardSubtitle>
                  <IonCardSubtitle>
                    工作日期 : {formatDateTime(job.start_date)} 至 <br></br>
                    {formatDateTime(job.end_date)}{" "}
                  </IonCardSubtitle>
                  <IonCardSubtitle>
                    工作時間:{job.daily_start_time} 至 {job.daily_end_time}
                  </IonCardSubtitle>

                  <IonCardSubtitle>
                    所需人數:{job.require_worker_num}
                  </IonCardSubtitle>
                  <IonCardSubtitle>薪金:{job.salary}</IonCardSubtitle>
                </IonCardHeader>
                <IonButton color="success" onClick={(e) => accept(job.id)}>
                  接受
                </IonButton>
                <IonButton color="danger" onClick={(e) => reject(job.id)}>
                  {" "}
                  拒絕
                </IonButton>
              </IonCard>
            </div>
          ))
        )}
      </IonContent>
    </IonPage>
  );
};

export default JobRequest;
