import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonThumbnail,
  IonTitle,
  IonToolbar,
  useIonAlert,
  useIonRouter,
  useIonToast,
} from "@ionic/react";
import { useParams } from "react-router";
import { useGet } from "../hooks/use-get";
import { useResource } from "../hooks/use-resource";
import { routes } from "../routes";
import "./EmployeeProfile.css";
import { KB } from "@beenotung/tslib/size";
import { selectImage } from "@beenotung/tslib/file";
import {
  compressMobilePhoto,
  dataURItoBlob,
  resizeBase64WithRatio,
} from "@beenotung/tslib/image";
import { env } from "../env";
import { useState } from "react";
import { useRole } from "../hooks/useRole";
import { FileUploader } from "react-drag-drop-files";

const EmployeeProfile: React.FC = () => {
  const { user_id } = useParams<{ user_id: string }>();
  const [present] = useIonAlert();
  const [presents] = useIonToast();
  const router = useIonRouter();
  const [icon, setIcon] = useState(env.S3);
  const [attach, setAttach] = useState(false);

  let enable = useRole()?.enable;

  async function submitProfile() {
    if(!profile.state.nickname){
      present("please enter nickname", [{ text: "Close" }]);
      return
    }
    if(!profile.state.fullname){
      present("please enter fullname", [{ text: "Close" }]);
      return
    }
    if(profile.state.district_list.length === 0){
      present("please choose district", [{ text: "Close" }]);
      return
    }
    if(!profile.state.gender){
      present("please enter gender", [{ text: "Close" }]);
      return
    }
    if(!profile.state.salary){
      present("please enter salary", [{ text: "Close" }]);
      return
    }
    let json = await profile.saveWithFile();
    if (json.error) {
      present("Fail to save profile: " + json.error, [{ text: "Close" }]);
    } else {
      presents("saved profile", 3000);
      if (!enable) {
        router.push(routes.employee.HomePage);
      }
    }
  }
  const profile = useResource("/employee/profile/" + user_id, {
    error: "",
    nickname: "",
    gender: "",
    salary: "",
    district_list: [],
    bank_account: "",
    fullname: "",
    icon: "",
    attach: "",
    icon_file: null as File | undefined | null,
    attach_file: null as File | undefined | null,
  });

  const list = useGet("/district", {
    error: "",
    list: [] as {
      id: number;
      name: string;
    }[],
  });

  const addPhoto = async () => {
    let files = await selectImage({ multiple: false, accept: "image/*" });
    let file = files[0]
    if(!file){return}
      let dataUrl = await compressMobilePhoto({
        image: file,
        maximumSize: 300 * KB,
      });
      dataUrl = await resizeBase64WithRatio(
        dataUrl,
        { width: 128, height: 128 },
        "with_in"
      );
      let blob = dataURItoBlob(dataUrl);
      file = new File([blob], file.name, {
        type: blob.type,
        lastModified: file.lastModified,
      });
      setIcon("");
      profile.patchDraftData({ icon_file: file });
      profile.patchDraftData({ icon: dataUrl });
  
  };

  return (
    <IonPage className="empPro">
      <IonHeader>
        <IonToolbar>
          {enable ? (
            ""
          ) : (
            <IonButtons slot="start">
              <IonBackButton defaultHref={routes.register}></IonBackButton>
            </IonButtons>
          )}
          <IonTitle>僱員資料</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        {profile.render((json) => {
          return (
            <>
              <div className="icon">
                <IonThumbnail>
                  <img
                    alt="icon"
                    src={icon + json.icon}
                    onError={(e) => {
                      e.currentTarget.src = "./assets/image/companylogo2nd.png"}}
                  />
                </IonThumbnail>
              </div>
              <IonList lines="full">
                <IonItem>
                  <IonButton onClick={addPhoto}>上傳頭像</IonButton>
                </IonItem>
                <IonItem>
                  <IonLabel>全名:</IonLabel>
                  <IonInput
                    className="ion-text-right"
                    value={json.fullname}
                    placeholder="Enter Fullname"
                    onIonChange={(e) =>
                      profile.patchDraftData({ fullname: e.detail.value! })
                    }
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel>稱呼:</IonLabel>
                  <IonInput
                    className="ion-text-right"
                    value={json.nickname}
                    placeholder="Enter Nickname"
                    onIonChange={(e) =>
                      profile.patchDraftData({ nickname: e.detail.value! })
                    }
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel>性別</IonLabel>
                  <IonSelect
                    value={json.gender}
                    onIonChange={(e) =>
                      profile.patchDraftData({ gender: e.detail.value })
                    }
                  >
                    {" "}
                    <IonSelectOption value={"m"}>男</IonSelectOption>
                    <IonSelectOption value={"f"}>女</IonSelectOption>
                  </IonSelect>
                </IonItem>
                <IonItem>
                  <IonLabel>工資</IonLabel>
                  <IonInput
                    className="ion-text-right"
                    value={json.salary}
                    type="number"
                    placeholder="Enter Salary"
                    onIonChange={(e) =>
                      profile.patchDraftData({ salary: e.detail.value! })
                    }
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel>銀行帳號</IonLabel>
                  <IonInput
                    className="ion-text-right"
                    value={json.bank_account}
                    type="text"
                    placeholder="Enter bank_account"
                    onIonChange={(e) =>
                      profile.patchDraftData({ bank_account: e.detail.value! })
                    }
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel>上班地區 *</IonLabel>
                  <IonSelect
                    value={json.district_list}
                    multiple
                    onIonChange={(e) =>
                      profile.patchDraftData({ district_list: e.detail.value })
                    }
                  >
                    {Array.isArray(list.state) &&
                      list.state.map((v: any) => (
                        <IonSelectOption key={v.id} value={v.id}>
                          {v.name}
                        </IonSelectOption>
                      ))}
                  </IonSelect>
                </IonItem>
                <IonItem>
                  <IonLabel>附件</IonLabel>
                  {json.attach ? (
                    "已上傳附件"
                  ) : (
                    <IonButton onClick={(e) => setAttach(true)} hidden={attach}>
                      上傳新附件
                    </IonButton>
                  )}
                  {attach ? (
                    <FileUploader
                      handleChange={(file: any) =>
                        profile.patchDraftData({ attach_file: file })
                      }
                      name="file"
                      types={["PDF"]}
                      value={json.attach_file}
                    />
                  ) : (
                    ""
                  )}
                </IonItem>
              </IonList>
              <IonButton expand="block" onClick={submitProfile}>
                保存資料
              </IonButton>
            </>
          );
        })}
      </IonContent>
    </IonPage>
  );
};

export default EmployeeProfile;
