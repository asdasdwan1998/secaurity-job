import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonSegment,
  IonSegmentButton,
  IonToolbar,
  useIonAlert,
  useIonToast,
} from "@ionic/react";
import { call, logIn, mail } from "ionicons/icons";
import { useDispatch, useSelector } from "react-redux";
import { useObject } from "../hooks/use-object";
import { loginThunk } from "../redux/auth/thunk";
import "./UserLogin.css";
import { routes } from "../routes";
import { RootState } from "../redux/state";
import { useEffect } from "react";
import { post } from "../api";
import { to_full_hk_mobile_phone, is_email } from "@beenotung/tslib/validate";
import { storage } from '../storage'

const UserLogin: React.FC = () => {
  const { set, value } = useObject({
    email: "",
    tel: "",
    passcode: "",
    type: "email",
    sent: 0,
    waitTime: 0,
    invite_code: "",
    open_invite: false
  });
  const [present] = useIonAlert();
  const [presents] = useIonToast();
  const result = useSelector((state: RootState) => state.auth.loginResult);
  useEffect(() => {
    if (result.type === "fail") {
      present(result.message, [{ text: "Close" }]);
    }
  }, [result, present]);

  useEffect(() => {
    const interval = setInterval(() => {
      set("waitTime", value.waitTime - 1);
    }, 1000);
    if (value.waitTime <= 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [value.waitTime,set]);


  const dispatch = useDispatch();
  async function showSuccess(){
    presents("login successfully", 5000);
  }
  async function emailLogin() {
    if (!is_email(value.email)) {
      present("Error Email", [{ text: "Close" }]);
      return;
    }
    dispatch(
      loginThunk({ email: value.email, passcode: value.passcode, invite_code: value.invite_code }, showSuccess)
    );
  }
  async function mobileLogin() {
    if (!to_full_hk_mobile_phone(value.tel)) {
      present("Error Mobile", [{ text: "Close" }]);
      return;
    }
    dispatch(
      loginThunk({ tel: value.tel, passcode: value.passcode, invite_code: value.invite_code }, showSuccess)
    );
  }

  async function sendEmail() {
    if (!is_email(value.email)) {
      present("Error Email", [{ text: "Close" }]);
      return;
    }
    set("waitTime", 60)
    let json = await post("/login/email", { email: value.email });
    if (value.email.endsWith("@example.net")) {
      json.error = ""
    }
    if (json.error) {
      present(json.error, [{ text: "Close" }]);
    } else {
      set("sent", value.sent + 1);
      storage.invite_code = value.invite_code
      storage.email = value.email
      // localStorage.setItem('email', value.email)
      present(json.message, [{ text: "Close" }]);
    }
  }

  async function sendTelephone() {
    if (!to_full_hk_mobile_phone(value.tel)) {
      present("Error Telephone", [{ text: "Close" }]);
      return;
    }
    set("waitTime", 60)
    let json = await post("/login/tel", { tel: value.tel });
    if (json.error) {
      present(json.error, [{ text: "Close" }]);
    } else {
      set("sent", value.sent + 1);
      storage.invite_code = value.invite_code
      storage.tel = value.tel
      // localStorage.setItem('mobile', value.tel)
      present(json.message, [{ text: "Close" }]);
    }
  }

  let requireVerifyCode = value.type === "email" ? sendEmail : sendTelephone;
  let LoginVerifyCode = value.type === "email" ? emailLogin : mobileLogin;
  async function openInvite() {
    if (value.open_invite === false) { set("open_invite", true) } else {
      set("open_invite", false)
    }
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.welcome}></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div className="login-bg"></div>
        <div className="ion-padding">
          <div style={{display: "flex", 'alignItems' : 'center' , "justifyContent" : 'space-between'}}>
          <h2 className="login-list-header" style={{'marginTop' : '10px'}}>WELCOME !</h2>
          <IonButton slot="end" onClick={() => openInvite()}>{value.open_invite === false ? '註冊' : '登入'}</IonButton></div>
          <IonSegment
            onIonChange={(e) => set("type", e.detail.value || "")}
            value={value.type}
          >
            <IonSegmentButton value="email">
              <IonLabel>
                <IonIcon icon={mail} slot="start" />
                &nbsp;Mail
              </IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value="telephone">
              <IonLabel>
                <IonIcon icon={call} slot="start" />
                &nbsp;Telephone
              </IonLabel>
            </IonSegmentButton>
          </IonSegment>
          <IonList lines="none" class="ion-margin login-list">
            {value.open_invite === true ?
              <IonItem class="ion-margin">
                <IonInput placeholder="Enter invite code (option)" maxlength={6} type="tel"></IonInput>
              </IonItem> : ""}
            <IonItem class="ion-margin">
              {value.type === "email" ? (
                <IonInput
                  placeholder="Enter Email"
                  type="email"
                  value={value.email}
                  onIonChange={(e) => set("email", e.detail.value || "")}
                ></IonInput>
              ) : (
                <IonInput
                  placeholder="Enter Telephone"
                  type="number"
                  value={value.tel}
                  onIonChange={(e) => set("tel", e.detail.value || "")}
                ></IonInput>
              )}
            </IonItem>
            {value.waitTime !== 0 ?
              <IonButton class="ion-margin" expand="block" disabled={true}>
                重新發送，請等待{value.waitTime}秒
              </IonButton> :
              <IonButton class="ion-margin" expand="block" onClick={requireVerifyCode}>
                Send the verification code
              </IonButton>}
            {value.sent > 0 ? (
              <IonItem class="ion-margin">
                <IonInput
                  placeholder="Enter verification code"
                  type="text"
                  value={value.passcode}
                  onIonChange={(e) => set("passcode", e.detail.value || "")}
                ></IonInput>
              </IonItem>
            ) : (
              ""
            )}

            <div className="ion-text-end">
              <IonButton
                type="submit"
                shape="round"
                color="dark"
                onClick={LoginVerifyCode}
              >
                {value.open_invite === false ? 'SIGN IN' : 'SIGN UP'}
                <IonIcon icon={logIn} slot="end"></IonIcon>
              </IonButton>
            </div>
          </IonList>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default UserLogin;
