import { IonButton, IonCard, IonCardContent, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonPage, IonRow, IonTitle, IonToolbar, useIonToast } from '@ionic/react';
import { chevronForwardOutline, logOut, personAdd, storefront } from 'ionicons/icons';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router';
import { useGet } from '../hooks/use-get';
import { useRole } from '../hooks/useRole';
import { logoutThunk } from '../redux/auth/thunk';
import { routes } from '../routes';
import './RegisterPage.css'

const RegisterPage: React.FC = () => {
  const [present] = useIonToast();

  const dispatch = useDispatch();
  async function logout(){
    dispatch(
      logoutThunk(showSuccess)
    );
  }
  async function showSuccess(){
    present("logout sucessfully", 5000);
  }

  let employee_id = useRole()?.id
  let nickname = useRole()?.nickname
  let company_id = useRole()?.company_id

  useGet(!nickname ? "skip" : "/update/token" ,{
    error: ""})



  if(!employee_id){
    return <Redirect to= {routes.login}></Redirect>
  }
  let employee_route = routes.employeeProfile(employee_id)
  return (
    <IonPage className='register'>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Register</IonTitle>
          <IonButton slot="end" onClick={logout}>
            <IonIcon slot="end" icon={logOut}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
       <h4 className='p-4 ion-text-center'>{nickname ? "等待審核中":"請提供申請資料 須時審核"}</h4>
       <IonGrid fixed>
         <IonRow>
           {(!nickname) || (nickname && !company_id) ? <IonCol size='12'>
              <IonCard routerLink={employee_route}>
                <IonCardContent >
                  <div className='title'>
                    <div style={{'display': 'flex'}}>
                    <IonIcon icon={personAdd} className="f1"></IonIcon>
                    <p>立即登記成為<br/>僱員</p>
                    </div>
                    <IonIcon icon={chevronForwardOutline} className="fr icon-go"></IonIcon>
                    <h2>Employee</h2>
                    </div>
                </IonCardContent>
              </IonCard>
           </IonCol> : null}
           {!nickname || company_id ? <IonCol size='12'>
              <IonCard routerLink={routes.createCompany}>
                <IonCardContent>
                  <div className='title'>
                  <div style={{'display': 'flex'}}>
                    <IonIcon icon={storefront} className="f1"></IonIcon>
                    <p>立即登記成為<br/>僱主</p>
                    </div>
                    <IonIcon icon={chevronForwardOutline} className="fr icon-go"></IonIcon>
                    <h2>Company</h2>
                    </div>
                </IonCardContent>
              </IonCard>
           </IonCol>: null}
           
           </IonRow></IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegisterPage;
