import { IonText } from '@ionic/react'

export function ErrorMessage(props: { error: string }) {
  return (
    <>
      {props.error ? (
        <p className="ion-text-center">
          <IonText color="danger">{props.error}</IonText>
        </p>
      ) : null}
    </>
  )
}
