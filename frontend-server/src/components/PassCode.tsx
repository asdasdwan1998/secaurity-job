import { useIonToast, useIonRouter } from "@ionic/react";
import { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { loginThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import { routes } from "../routes";
import { storage } from '../storage'

export function VerifyPasscodePage() {
  let params = useParams<{ passcode: string }>();
  let passcode = params.passcode;
  const [present] = useIonToast();
  const router = useIonRouter();
  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const result = useSelector((state: RootState) => state.auth.loginResult);
  useEffect(() => {
    if (result.type === "fail") {
      setError(result.message);
    } else if (result.type === "success") {
      router.push(routes.welcome);
    }
  }, [result, router]);

  const showSuccess = useCallback(()=>{ present("login successfully",5000) },[present])

  useEffect(() => {
    if (!passcode) {
      setError("missing passcode");
      return;
    }
    setError("");
    let invite_code = storage.invite_code || undefined
    let email = storage.email
    let tel = storage.tel
    if (email) {
      dispatch(loginThunk({ email, passcode, invite_code },showSuccess));
    } else if (tel) {
      dispatch(loginThunk({ tel, passcode, invite_code },showSuccess));
    } else {
      if (!email && !tel) {
        setError("missing email or tel");
        return;
      }
    }
  }, [passcode,dispatch,showSuccess]);
  return (
    <div>
      {error ? (
        <>
          <p style={{ color: "red" }}>{error}</p>
          <Link to="/login">Try again</Link>
        </>
      ) : null}
    </div>
  );
}
