import { IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react'
import { routes } from './routes'
import { personCircle, home, chatbubbles } from 'ionicons/icons';
import { useRole } from './hooks/useRole';

export const EmployeeTabBar = () => {

    let employee_id = useRole()?.id
    let employee_route = routes.employee.Profile(employee_id || 0)
    
    return (
        <IonTabBar slot="bottom">
            <IonTabButton tab="homepage" href={routes.employee.HomePage}>
                <IonIcon icon={home} />
                <IonLabel>home</IonLabel>
            </IonTabButton>
            <IonTabButton tab="bookshelf" href={routes.employee.messageList}>
                <IonIcon icon={chatbubbles} />
                <IonLabel>message</IonLabel>
            </IonTabButton>
            <IonTabButton tab="profile" href={employee_route}>
                <IonIcon icon={personCircle} />
                <IonLabel>setting</IonLabel>
            </IonTabButton>
        </IonTabBar>
    )
}


export default EmployeeTabBar