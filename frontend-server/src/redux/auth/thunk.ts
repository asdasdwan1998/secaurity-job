import { post } from "../../api";
import { AppDispatch } from "../action";
import { RootState } from "../state";
import { logoutAction, setLoginResultAction, setRegisterResultAction  } from "./action";
import { storage } from '../../storage'

export function logoutThunk(showSuccess : ()=>void){
    return async (dispatch: AppDispatch, getState:()=>RootState)=> {
        showSuccess()
        setTimeout(() => {
        storage.token = null
        dispatch(logoutAction())
        },1000)
    }
}

export function loginThunk(user: { email: string; passcode: string ; invite_code?: string} | { tel: string; passcode: string ; invite_code?: string}, showSuccess : ()=>void){
    return async (dispatch: AppDispatch, getState:()=>RootState)=> {
        let json = await post('/login/passcode',user)
        if(json.error){
            dispatch(setLoginResultAction({type:'fail',message:json.error}))
        } else {
            showSuccess()
            setTimeout(() => {
                storage.token = json.token
                storage.invite_code = null
                storage.tel = null
                storage.email = null
                dispatch(setLoginResultAction({type:'success',token: json.token}))
            },1000)
        }
    }
}

export function registerThunk(user:  { email: string ; passcode: string } | { tel: string; passcode: string ; invite_code?: string}){
    return async (dispatch: AppDispatch, getState:()=>RootState)=> {
        let json = await post('/register/passcode',user)
        if(json.error){
            dispatch(setRegisterResultAction({type:'fail',message:json.error}))
        } else {
            storage.token = json.token
            dispatch(setRegisterResultAction({type:'success',token: json.token}))
        }
    }
}

export function updateToken(token: string){
    return async (dispatch: AppDispatch, getState:()=>RootState)=> {
        // localStorage.setItem('token',token)
        storage.token = token
        dispatch(setLoginResultAction({type:'success',token: token}))
    }
}

// export function postItem(item : {title: string}){
//     return async (dispatch: AppDispatch, getState:()=>RootState)=> {
//         let token = getState().auth.user?.token
//         if(!token){
//             return
//         }
//         let json = await post('/item',item)
//         if(json.error){
//            return
//         } else {
//             return
//         }
//     }
// }