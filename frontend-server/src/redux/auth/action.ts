import { APIResultToken } from "./state"


  export function logoutAction() {
    return { type: '@@Auth/logout' as const }
  }
  
export function setLoginResultAction(result: APIResultToken){
  return {type:'@@Auth/setLoginResult' as const,result}
}
export function setRegisterResultAction(result: APIResultToken){
  return {type:'@@Auth/setRegisterResult' as const,result}
}


  export type AuthAction =
    | ReturnType<typeof setLoginResultAction>
    | ReturnType<typeof setRegisterResultAction>
    | ReturnType<typeof logoutAction>
  