export type AuthState = {
    user: null | AuthUser
    registerResult: APIResultToken
    loginResult: APIResultToken
  }
  
  export type APIResultToken =
    | { type: 'idle' }
    | { type: 'success'; token: string}
    | { type: 'fail'; message: string }

  export type AuthUser = {
    id: number
    token:string
    nickname: string
    is_admin: boolean 
    is_employee : boolean
    company_id?: number
    enable: boolean
  }
  