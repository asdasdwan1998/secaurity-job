export let env = {
    API_ORIGIN: window.location.origin === 'http://localhost:3000' ? 'http://localhost:8800' : 'https://app.dmoe.fun',
    S3 : window.location.origin === 'http://localhost:3000' ? 'http://localhost:8800/' :'https://guard-usercontent.s3.us-east-2.amazonaws.com/'
}