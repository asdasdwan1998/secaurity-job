import { IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react'
import { routes } from './routes'
import { personCircle, home, chatbubbles } from 'ionicons/icons';

export const CompanyTabBar = () => {
    return (
        <IonTabBar slot="bottom">
            <IonTabButton tab="homepage" href={routes.company.home}>
                <IonIcon icon={home} />
                <IonLabel>Home</IonLabel>
            </IonTabButton>
            <IonTabButton tab="bookshelf" href={routes.company.messageList}>
                <IonIcon icon={chatbubbles} />
                <IonLabel>message</IonLabel>
            </IonTabButton>
            <IonTabButton tab="profile" href={routes.company.setting}>
                <IonIcon icon={personCircle} />
                <IonLabel>setting</IonLabel>
            </IonTabButton>
        </IonTabBar>
    )
}

export default CompanyTabBar