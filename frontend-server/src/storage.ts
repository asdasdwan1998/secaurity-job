export let KEYS = {
    email : 'email',
    tel: 'tel',
    token : 'token',
    invite_code : 'invite_code'
}

export class Storage {
    get invite_code(){
        return localStorage.getItem(KEYS.invite_code)
    }
    set invite_code(value: string | null){
        localStorage.setItem(KEYS.invite_code, value || '')
    }

    get email(){
        return localStorage.getItem(KEYS.email)
    }
    set email(value: string | null){
        localStorage.setItem(KEYS.email, value || '')
    }
    get token(){
        return localStorage.getItem(KEYS.token)
    }
    set token(value: string | null){
        localStorage.setItem(KEYS.token, value || '')
    }
    get tel(){
        return localStorage.getItem(KEYS.tel)
    }
    set tel(value: string | null){
        localStorage.setItem(KEYS.tel, value || '')
    }

}

export let storage = new Storage()