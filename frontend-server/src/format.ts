export function formatDateTime(s: string) {
    return new Date(s).toLocaleString()
}