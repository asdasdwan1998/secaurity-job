import { IonRouterOutlet, IonTabs, IonSplitPane } from "@ionic/react";
import { Route, Redirect } from "react-router";
import { VerifyPasscodePage } from "./components/PassCode";
import { useRole } from "./hooks/useRole";
import attendancePayment from "./pages/admin/attendancePayment";
import companyApproval from "./pages/admin/companyApproval";
import companyList from "./pages/admin/companyList";
import employeeApproval from "./pages/admin/employeeApproval";
import employeeList from "./pages/admin/userList";
import Menu from "./pages/admin/Menu";
import projectDetails from "./pages/admin/projectDetails";
import reportStatement from "./pages/admin/reportStatement";
import rewardSystem from "./pages/admin/rewardSystem";
import NewProject from "./pages/company/applyNewProject";
import CompanyDetail from "./pages/company/companyDetail";
import CompanyHomePage from "./pages/company/HomePage";
import CompanySetting from "./pages/company/setting";
import projectManage from "./pages/company/projectManage";
import createCompany from "./pages/company/create-company";
import TimeTable from "./pages/employee/Timetable";
import jobEmployeeList from "./pages/company/jobEmployeeListPage"
import timeTable from "./pages/company/timetable";
import userDetail from "./pages/company/userDetail";
import EmployeeHomePage from "./pages/employee/HomePage";
import JobRequest from "./pages/employee/JobRequest";
import Salary from "./pages/employee/Salary";
import Message from "./pages/Message";
import MessageList from "./pages/MessageList";
import NotMatchPage from "./pages/NotMatchPage";
import RegisterPage from "./pages/RegisterPage";
import UserLogin from "./pages/UserLogin";
import WelcomePage from "./pages/WelcomePage";
import CompanyTabBar from "./TabBarCompany";
import EmployeeTabBar from "./TabBarEmployee";
import EmployeeProfile from "./pages/EmployeeProfile";

export let routes = {
  employee: {
    HomePage: "/employee/HomePage",
    Timetable: "/employee/Timetable",
    messageList: "/employee/messageList",
    Salary: "/employee/Salary",
    JobRequest: "/employee/JobRequest",
    Profile:(user_id:number|string)=> "/employee/Profile/" + user_id,
  },
  company: {
    messageList: "/company/tab/messageList",
    home: "/company/tab/home",
    setting: "/company/tab/setting",
    CompanyDetail: "/company/CompanyDetail",
    projectManage: "/company/projectManage",
    userDetail: "/company/userDetail",
    timetable: "/company/timetable",
    NewProject: "/company/applyNewProject",
    jobEmployeeList:(job_id:number|string)=> "/company/employeelist/" + job_id,
    createCompany:"/company/create-company"
  },
  admin: {
    companyList: "/admin/companyList",
    userList: "/admin/userList",
    companyApproval: "/admin/companyApproval",
    employeeApproval: "/admin/employeeApproval",
    projectDetails: "/admin/projectDetails",
    attendancePayment: "/admin/attendancePayment",
    rewardSystem: "/admin/rewardSystem",
    basicData: "/admin/basicData",
    reportStatement: "/admin/reportStatement",
  },

  welcome: "/welcome",
  login: "/login",
  register: "/register",
  profile: "/profile",
  settings: "/settings",
  about: "/about",
  terms: "/terms",
  privacy: "/privacy",
  messageList: "/messageList",
  message:(type: string, id:number|string)=>  "/message/" + type + "/" + id,
  employeeProfile:(user_id:number|string)=> "/employeeProfile/" + user_id,
  createCompany: "/createCompany",
};

const GuestRoute = (props: {
  exact?: boolean;
  path: string;
  component: React.FC;
}) => {
  const user = useRole();
  const Component = props.component;
  return (
    <Route exact={props.exact} path={props.path}>
      {!user ? <Component /> : <Redirect to=
        {!user.enable ? routes.register :
          user.company_id ? routes.company.home :
            user.is_admin ? routes.admin.companyList :
              routes.employee.HomePage} />}

    </Route>
  );
};

const UserRoute = (props: {
  exact?: boolean
  path: string
  component: React.FC
}) => {
  const user = useRole()
  const Component = props.component
  return (
    <Route exact={props.exact} path={props.path}>
      {user ? <Component /> : <Redirect to={routes.welcome} />}
    </Route>
  )
}

const EmployeeRoute = (props: {
  exact?: boolean;
  path: string;
  component: React.FC;
}) => {
  const user = useRole()
  const Component = props.component;
  return (
    <Route exact={props.exact} path={props.path}>
      {user?.is_employee ?  <Component /> : <Redirect to={routes.login} />}
    </Route>
  );
};

const CompanyRoute = (props: {
  exact?: boolean;
  path: string;
  component: React.FC;
}) => {
  const user = useRole()
  const Component = props.component;
  return (
    <Route exact={props.exact} path={props.path}>
      {user?.company_id ?  <Component /> : <Redirect to={routes.login} />}
    </Route>
  );
};


export const Routes = () => {
  return (
    <>
      <IonRouterOutlet>
        <Route
          path="/login/passcode/:passcode"
          component={VerifyPasscodePage}
        />
        {/* <Route path="/employee/profile/:user_id" component={EmployeeProfile} /> */}
        <Route exact path="/">
          <Redirect to={routes.welcome} />
          {/* <DefaultPage /> */}
        </Route>
        <GuestRoute exact path={routes.welcome} component={WelcomePage} />
        <GuestRoute exact path={routes.login} component={UserLogin} />
        <UserRoute path={routes.register} component={RegisterPage} />
        <UserRoute path={routes.messageList} component={MessageList} />
        <UserRoute path={routes.message(':type',':id')} component={Message} />
        <UserRoute path={routes.employeeProfile(':user_id')} component={EmployeeProfile} />
        <UserRoute path={routes.createCompany} component={createCompany} />
        <Route path="/employee">
          <IonTabs>
            <IonRouterOutlet>
              <EmployeeRoute path={routes.employee.HomePage} component={EmployeeHomePage}/>
              <EmployeeRoute path={routes.employee.Salary} component={Salary} />
              <EmployeeRoute path={routes.employee.JobRequest} component={JobRequest} />
              <EmployeeRoute path={routes.employee.Profile(':user_id')} component={EmployeeProfile} />
              <EmployeeRoute path={routes.employee.Timetable} component={TimeTable} />
              <EmployeeRoute path={routes.employee.messageList} component={MessageList} />
            </IonRouterOutlet>
            {EmployeeTabBar()}
          </IonTabs>
        </Route>



          <Route path="/company/tab">
            <IonTabs>
              <IonRouterOutlet>
                <CompanyRoute
                  exact
                  path={routes.company.home}
                  component={CompanyHomePage}
                />
                <CompanyRoute
                  exact
                  path={routes.company.setting}
                  component={CompanySetting}
                />
                <CompanyRoute
                  path={routes.company.messageList}
                  component={MessageList}
                />
              </IonRouterOutlet>
              {CompanyTabBar()}

            </IonTabs>
          </Route>

          <CompanyRoute
            path={routes.company.projectManage}
            component={projectManage}
          />

          <CompanyRoute path={routes.company.userDetail} component={userDetail} />

          <CompanyRoute
            path={routes.company.CompanyDetail}
            component={CompanyDetail}
          />
          <CompanyRoute path={routes.company.timetable} component={timeTable} />
          <CompanyRoute path={routes.company.NewProject} component={NewProject} />
          <CompanyRoute path={routes.company.jobEmployeeList(':job_id')} component={jobEmployeeList} />
          <CompanyRoute path={routes.company.createCompany} component={createCompany} />


        <Route path="/admin">

          <IonSplitPane contentId="main">
            <Menu />
            <IonRouterOutlet id="main">
              <Route path={routes.admin.companyList} component={companyList} />
              <Route
                path={routes.admin.companyList}
                component={companyList}
              />
              <Route
                path={routes.admin.userList}
                component={employeeList}
              />
              <Route
                path={routes.admin.companyApproval}
                component={companyApproval}
              />
              <Route
                path={routes.admin.employeeApproval}
                component={employeeApproval}
              />
              <Route
                path={routes.admin.projectDetails}
                component={projectDetails}
              />
              <Route
                path={routes.admin.attendancePayment}
                component={attendancePayment}
              />
              <Route
                path={routes.admin.rewardSystem}
                component={rewardSystem}
              />
              <Route path={routes.admin.reportStatement}
                component={reportStatement}
              />
            </IonRouterOutlet>
          </IonSplitPane>
        </Route>
        <Route component={NotMatchPage} />

      </IonRouterOutlet>
    </>
  );
};
