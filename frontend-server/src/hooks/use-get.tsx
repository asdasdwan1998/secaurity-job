import { IonProgressBar } from '@ionic/react'
import { useCallback, useEffect, useState } from 'react'
import useStorageState from 'react-use-storage-state'
import { ErrorMessage } from '../components/ErrorMessage'
import { get } from '../api'

export function useGet<T extends { error: string }>(url: string, initial: T, storage?: Storage) {
  const [state, setState] = useStorageState(url, initial, storage)
  const [isFirst, setIsFirst] = useStorageState('isFirst:' + url, true)
  const [isLoading, setIsLoading] = useState(false)

  const download = useCallback(async function download() {
    setIsLoading(true)
    let json = await get(url)
      if (!json.error || !(json.error.includes('NetworkError')|| json.error.includes('Failed to fetch')) || isFirst) {
        setState(json)
      }
      console.log(json)
      if (isFirst) {
        setIsFirst(false)
      }
      setIsLoading(false)
  },[url,setIsFirst, setState, isFirst])

  useEffect(() => {
    if(url === "skip"){return}
    download()
  }, [url, download])

  function render(fn: (state: Omit<T, 'error'>) => any) {
    return (
      <>
        {state.error ? (
          <ErrorMessage error={state.error} />
        ) : (
          <>
            <div
              style={{
                transition: 'opacity 0.15s',
                opacity: isLoading ? 1 : 0,
              }}
            >
              <IonProgressBar type="indeterminate" />
            </div>
            {fn(state)}
          </>
        )}
      </>
    )
  }
  return { state, setState, render, refresh: download }
}
