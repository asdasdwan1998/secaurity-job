import { useIonToast } from '@ionic/react'
import { useState } from 'react'
import { ErrorMessage } from '../components/ErrorMessage'
import { post, postWithFile } from '../api'

export function usePost<T>(url: string) {
  const [error, setError] = useState('')
  const [present] = useIonToast()
  return {
    showToast(message: string) {
      present(message, 3000)
    },
    error,
    renderError() {
      return <ErrorMessage error={error} />
    },
    post: async (body: object, successCallback?: (json: T) => void) => {
      let json = await post(url, body)
      setError(json.error)
      if (!json.error) {
        successCallback?.(json)
      }
    },
    postFile: async (body: object, successCallback?: (json: T) => void) => {
      let formData = new FormData()
      let data = body as any
      for (let key in data) {
      let value = data[key]
      formData.append(key, value)
    }
      let json = await postWithFile(url, formData)
      setError(json.error)
      if (!json.error) {
        successCallback?.(json)
      }
    },
  }
}

// window.addEventListener('offline', () => {})
// window.addEventListener('online', () => {})
