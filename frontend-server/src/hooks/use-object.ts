import { useCallback, useState } from 'react'

export const EmptyObject = {}

export const useObject = <T>(initialValue: T) => {
  const [state, setState] = useState(initialValue)
  const patch = useCallback(
    (partial: Partial<T>) => setState(state => ({ ...state, ...partial })),
    [setState],
  )
  const set = useCallback(
    <K extends keyof T>(key: K, value: T[K]) => {
      patch({ [key]: value } as any)
    },
    [patch],
  )
  const reset = useCallback(
    () => setState(initialValue),
    [setState, initialValue],
  )
  return {
    value: state,
    patch,
    set,
    reset,
    setState,
  }
}